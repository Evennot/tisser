//
//  Processor.h
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Processor;
@class Bus;

@protocol ProcessorDelegate <NSObject>

- (void)processor:(Processor *)processor compilationError:(NSString *)errorMessage;
- (void)processorWillChangeProgram:(Processor *)processor;
- (BOOL)running;
- (void)endEditing;

@end

typedef enum : NSUInteger {
    CoreModeProcessor = 0,
    CoreModeStack = 1,
    CoreModeLUT = 2,
} CoreMode;

@interface Processor : NSObject <UITextViewDelegate>

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, setter = setView:) UIView *view;
@property (nonatomic, readonly) BOOL hasProgramUpdates;
@property (nonatomic, weak) id<ProcessorDelegate>delegate;
@property (nonatomic, weak) Bus *up;
@property (nonatomic, weak) Bus *down;
@property (nonatomic, weak) Bus *left;
@property (nonatomic, weak) Bus *right;
@property (nonatomic, getter = getProgramText, setter = setProgramText:) NSString *programText;
@property (nonatomic, assign, readonly) NSInteger stepCount;
@property (nonatomic, assign, readonly) NSInteger stepsWorked;
@property (nonatomic, setter = setMode:) CoreMode mode;

- (instancetype)initWithName:(NSString *)name;
- (void) reset;
- (void) step;
- (void)rebuildFramesWithK:(CGFloat)k;
+ (NSDictionary *)commandsDict;
- (NSNumber *) pickable;
- (NSNumber *) pick;
- (void) push:(NSNumber *)data;
- (void) stepIsOver;
@end
