//
//  ScenariosViewController.m
//  tisser
//
//  Created by Danila Parkhomenko on 10/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "ScenariosViewController.h"
#import "Scenario.h"
#import "ViewController.h"
#import "FontHelper.h"

@interface ScenariosViewController ()
@property (nonatomic, strong) NSArray *scenarios;
@property (nonatomic) NSInteger selectedScenario;
@property (nonatomic) NSInteger selectedProgram;
@property (nonatomic) NSArray *selectedScenarioPrograms;
@property (nonatomic) BOOL animatingClosing;
@property (nonatomic) BOOL animatingOpening;
@end

@implementation ScenariosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectedScenario = -1;
    _selectedScenarioPrograms = nil;
    NSError *error = nil;
    self.scenarios = [Scenario loadWithError:&error];
    self.table.delegate = self;
    self.table.dataSource = self;
    self.duplicateButton.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = self.scenarios.count + ((_selectedScenario >= 0)?_selectedScenarioPrograms.count:0);
    return rowCount;
}

- (BOOL)programRow:(NSInteger)row
{
    if (_selectedScenario >= 0) {
        return (row > _selectedScenario) && (row <= _selectedScenario + _selectedScenarioPrograms.count);
    }
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    if (_selectedScenario >= 0) {
        if (row > _selectedScenario) {
            if (row > _selectedScenario + _selectedScenarioPrograms.count) {
                row -= _selectedScenarioPrograms.count;
            } else {
                row -= _selectedScenario + 1;
                SelectedScenarioViewCell *cell = (SelectedScenarioViewCell *)[tableView dequeueReusableCellWithIdentifier:@"programCell"];
                cell.programName.font = [FontHelper defaultFontDecreasedBy:1.5];
                cell.programName.text = [NSString stringWithFormat:@"Config %ld", (long)row + 1];
                cell.stats.font = [FontHelper defaultFontDecreasedBy:1.5];
                cell.stats.text = _selectedScenarioPrograms[row][@"stats"];
                cell.loadButton.tag = row;
                cell.loadButton.titleLabel.font = [FontHelper defaultFontDecreasedBy:1.5];
                [cell.loadButton addTarget:self action:@selector(loadProgramTapped:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
            }
        }
    }
    ScenarioViewCell *cell = (ScenarioViewCell *)[tableView dequeueReusableCellWithIdentifier:@"scenarioCell"];
    if (row >= self.scenarios.count) {
        [cell setupFor:self.scenarios[self.scenarios.count - 1]];
    } else {
        [cell setupFor:self.scenarios[row]];
    }
    return cell;
}

- (void) loadProgramTapped:(UIButton *)button
{
    [self.master loadProgram:self.selectedScenarioPrograms[button.tag]
                     details:((Scenario *)self.scenarios[_selectedScenario]).details
                        path:@[@(_selectedScenario), @(button.tag)]];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self programRow:indexPath.row]) {
        return 44.0;
    }
    return 80.0;
}

- (void)animateInsertionAroundRow:(NSInteger)row
{
    NSMutableArray *indexPathsToInsert = [NSMutableArray array];
    _selectedScenario = row;
    _selectedScenarioPrograms = ((Scenario *)self.scenarios[_selectedScenario]).programsArray;
    for (NSInteger i = 0; i < _selectedScenarioPrograms.count; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:row + 1 + i
                                                         inSection:0]];
    }
    [self.table insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self tableView:self.table
//                                                                numberOfRowsInSection:0] - 1
//                                                            inSection:0],
//                                         [NSIndexPath indexPathForRow:[self tableView:self.table
//                                                                numberOfRowsInSection:0] - 2
//                                                            inSection:0],
//                                         [NSIndexPath indexPathForRow:[self tableView:self.table
//                                                                numberOfRowsInSection:0] - 3
//                                                            inSection:0],
//                                         ]
//                      withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    if ([self programRow:row]) {
        self.duplicateButton.enabled = YES;
        self.selectedProgram = row - _selectedScenario;
        return;
    }
    if (row == _selectedScenario) return;
    if (self.animatingClosing || self.animatingOpening) return;
    NSMutableArray *indexPathsToDelete = [NSMutableArray array];
    for (NSInteger i = _selectedScenarioPrograms.count; i--; ) {
        [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:_selectedScenario + 1 + i inSection:0]];
    }
    if (indexPathsToDelete.count > 0) {
        if (row > _selectedScenario) {
            row -= indexPathsToDelete.count;
        }
        self.animatingClosing = YES;
        _selectedScenario = -1;
        _selectedScenarioPrograms = nil;
        [UIView animateWithDuration:0.2 animations:^{
            [tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
        } completion:^(BOOL finished) {
            _selectedScenario = row;
            _selectedScenarioPrograms = ((Scenario *)self.scenarios[_selectedScenario]).programsArray;
            self.animatingClosing = NO;
            if (finished) {
                [self animateInsertionAroundRow:row];
            } else {
                [tableView reloadData];
            }
        }];
    } else {
        [self animateInsertionAroundRow:row];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([self programRow:indexPath.row]) {
//        return (self.selectedScenarioPrograms.count > 1);
//    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *mutablePrograms = [self.selectedScenarioPrograms mutableCopy];
    [mutablePrograms removeObjectAtIndex:indexPath.row - _selectedScenario - 1];
    ((Scenario *)self.scenarios[_selectedScenario]).programsArray = self.selectedScenarioPrograms = [mutablePrograms copy];
//    [Scenario removeProgramAtPath:@[_selectedScenario, indexPath.row - _selectedScenario - 1]];
//    if ([self programRow:indexPath.row]) {
//        return (self.selectedScenarioPrograms.count > 1);
//    }
}

- (IBAction)backTapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)importFromClipboard:(id)sender {
}

- (IBAction)duplicate:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

@implementation ScenarioViewCell

- (void) setupFor:(Scenario *)scenario
{
    self.caption.font = [FontHelper defaultFont];
    self.stats.font = [FontHelper defaultFontDecreasedBy:1.5];
    self.caption.text = scenario.caption;
    self.stats.text = scenario.details;
}

@end

@implementation SelectedScenarioViewCell

- (void) setupFor:(Scenario *)scenario
{
}

@end