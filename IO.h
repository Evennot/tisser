//
//  IO.h
//  tisser
//
//  Created by Danila Parkhomenko on 09/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Bus.h"

@interface IO : NSObject
@property (nonatomic, strong, setter = setView:) UIView *view;
@property (nonatomic, strong) InputBus *input0Data;
@property (nonatomic, strong) InputBus *input1Data;
@property (nonatomic, strong) OutputBus *output0Data;
@property (nonatomic, strong) OutputBus *output1Data;

@property (nonatomic, weak) UITextView *input0;
@property (nonatomic, weak) UITextView *input1;
@property (nonatomic, weak) UITextView *output0;
@property (nonatomic, weak) UITextView *output1;

- (BusOutcome) step;
- (void) reset;
- (void)rebuildFramesWithK:(CGFloat)k;

@end
