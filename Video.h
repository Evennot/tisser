//
//  Video.h
//  tisser
//
//  Created by Danila Parkhomenko on 11/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Bus.h"

@class Processor;

@interface Video : UIView

- (instancetype) initWithFrame:(CGRect)frame;
- (BOOL)postData:(NSNumber *)data from:(Processor *)processor;
- (void)reset;

@end


@interface VideoBus : Bus
@property (nonatomic, strong) Video *adapter;

- (BOOL)postData:(NSNumber *)data from:(Processor *)processor;

@end