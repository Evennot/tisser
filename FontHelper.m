//
//  FontHelper.m
//  tisser
//
//  Created by Danila Parkhomenko on 04/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "FontHelper.h"
#define FONTNAME @"KreditBack-Regular"
//@"DOS/EGA"
//@"Classic Console"
#define FONTSIZE 14.0
//19.0

@implementation FontHelper

+ (UIFont *)defaultFontDecreasedBy:(CGFloat)k
{
    return [UIFont fontWithName:FONTNAME size:FONTSIZE/k];
}

+ (UIFont *)defaultFont
{
    return [UIFont fontWithName:FONTNAME size:FONTSIZE];
}

@end
