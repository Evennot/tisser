//
//  IO.m
//  tisser
//
//  Created by Danila Parkhomenko on 09/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "IO.h"
#import "Bus.h"
#import "FontHelper.h"

@interface IO () {
    CGFloat lastK;
}

@end
@implementation IO

- (CGRect)rect:(NSInteger)rectIndex
{
    NSInteger total = 0;
    if (self.input0Data) total++;
    if (self.input1Data) total++;
    if (self.output0Data) total++;
    if (self.output1Data) total++;
    if (total == 0) return CGRectZero;
    CGSize size = self.view.bounds.size;
    return CGRectInset(CGRectMake(rectIndex*size.width/total, 0, size.width/total + ((rectIndex == total - 1)?0:1), size.height), 1, 1);
}

- (void)setView:(UIView *)view
{
    _view = view;
    view.backgroundColor = [UIColor whiteColor];
    NSInteger index = 0;
    if (self.input0Data) {
        UITextView *field = [[UITextView alloc] initWithFrame:[self rect:index++]];
        field.font = [FontHelper defaultFont];
        field.textColor = [UIColor grayColor];
        field.selectable = NO;
        field.editable = NO;
        field.backgroundColor = [UIColor blackColor];
        self.input0 = field;
        [view addSubview:field];
    }
    if (self.input1Data) {
        UITextView *field = [[UITextView alloc] initWithFrame:[self rect:index++]];
        field.font = [FontHelper defaultFont];
        field.textColor = [UIColor grayColor];
        field.selectable = NO;
        field.editable = NO;
        field.backgroundColor = [UIColor blackColor];
        self.input1 = field;
        [view addSubview:field];
    }
    if (self.output0Data) {
        UITextView *field = [[UITextView alloc] initWithFrame:[self rect:index++]];
        field.font = [FontHelper defaultFont];
        field.textColor = [UIColor grayColor];
        field.selectable = NO;
        field.editable = NO;
        field.backgroundColor = [UIColor blackColor];
        self.output0 = field;
        [view addSubview:field];
    }
    if (self.output1Data) {
        UITextView *field = [[UITextView alloc] initWithFrame:[self rect:index++]];
        field.font = [FontHelper defaultFont];
        field.textColor = [UIColor grayColor];
        field.selectable = NO;
        field.editable = NO;
        field.backgroundColor = [UIColor blackColor];
        self.output1 = field;
        [view addSubview:field];
    }
    [self updateTextWithK:1.0];
}

- (void) updateTextWithK:(CGFloat)k
{
    lastK = k;
    if (self.input0) {
        self.input0.attributedText = [self.input0Data attributedTextWithCaption:@"IN0" andK:k];
    }
    if (self.input1) {
        self.input1.attributedText = [self.input1Data attributedTextWithCaption:@"IN1" andK:k];
    }
    if (self.output0) {
        self.output0.attributedText = [self.output0Data attributedTextWithCaption:@"OUT0" andK:k];
    }
    if (self.output1) {
        self.output1.attributedText = [self.output1Data attributedTextWithCaption:@"OUT1" andK:k];
    }
}

- (BusOutcome) step {
    [self updateTextWithK:lastK];
    BusOutcome result;
    if (self.output0 == nil) {
        result = OutputDoneValid;
    } else {
        result = [self.output0Data ended];
    }
    switch (result) {
        case OutputNotDoneInvalid: return OutputNotDoneInvalid;
        case OutputNotDoneValid:
            if (self.output1) {
                switch ([self.output1Data ended]) {
                    case OutputDoneValid:
                    case OutputNotDoneValid: return OutputNotDoneValid;
                    default:
                        return OutputNotDoneInvalid;
                }
            }
            return OutputNotDoneValid;
        case OutputDoneValid:
            if (self.output1) {
                return [self.output1Data ended];
            }
            return OutputDoneValid;
        case OutputDoneInvalid:
        default:
            if (self.output1) {
                switch ([self.output1Data ended]) {
                    case OutputNotDoneInvalid:
                    case OutputNotDoneValid:
                        return OutputNotDoneValid;
                    default:
                        return OutputNotDoneInvalid;
                }
            }
            return OutputNotDoneInvalid;
    }
}

- (void) reset {
    [self updateTextWithK:lastK];
}

- (void)rebuildFramesWithK:(CGFloat)k {
    [self updateTextWithK:k];
    NSInteger i = 0;
    if (self.input0) [self.input0 setFrame:[self rect:i++]];
    if (self.input1) [self.input1 setFrame:[self rect:i++]];
    if (self.output0) [self.output0 setFrame:[self rect:i++]];
    if (self.output1) [self.output1 setFrame:[self rect:i++]];
}

@end
