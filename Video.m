//
//  Video.m
//  tisser
//
//  Created by Danila Parkhomenko on 11/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "Video.h"
#define VIDEOWIDTH 36
#define VIDEOHEIGHT 22

@interface Video () {
    char bytes[VIDEOWIDTH][VIDEOHEIGHT];
    NSInteger inputState;
    NSInteger inputSequence[8];
}
@end

@implementation Video

- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self reset];
    }
    return self;
}

- (void)reset
{
    memchr(bytes, 0, VIDEOWIDTH * VIDEOHEIGHT);
    inputState = 0;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect bounds = self.bounds;
    CGContextAddRect(context, bounds);
    CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
    CGContextFillPath(context);
    bounds = CGRectInset(bounds, 1, 1);
    CGFloat dx = bounds.size.width / VIDEOWIDTH;
    CGFloat dy = bounds.size.height / VIDEOHEIGHT;
    CGFloat xOffset = bounds.origin.x;
    CGFloat yOffset = bounds.origin.y;
    if (dx > dy) {
        dx = dy;
        xOffset += (bounds.size.width - dx * VIDEOWIDTH)*.5;
    } else {
        dy = dx;
        yOffset += (bounds.size.height - dy * VIDEOHEIGHT)*.5;
    }
    CGContextAddRect(context, CGRectInset(CGRectMake(xOffset, yOffset, dx * VIDEOWIDTH, dy * VIDEOHEIGHT), -1, -1));
    CGContextSetRGBStrokeColor(context, 0.6, 0.6, 0.6, 1.0);
    CGContextStrokePath(context);
    for (NSInteger c = 1; c <= 4; c++) {
        for (NSInteger i = VIDEOWIDTH; i--; ) {
            for (NSInteger j = VIDEOHEIGHT; j--; ) {
                if (bytes[i][j] == c) {
                    CGContextAddRect(context, CGRectMake(xOffset + (i * dx), yOffset + (j * dy), dx, dy));
                }
            }
        }
        switch (c) {
            case 1:
                CGContextSetRGBFillColor(context, 0.3, 0.3, 0.3, 1.0);
                break;
            case 2:
                CGContextSetRGBFillColor(context, 0.6, 0.6, 0.6, 1.0);
                break;
            case 3:
                CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
                break;
            case 4:
                CGContextSetRGBFillColor(context, 1.0, 0.1, 0.1, 1.0);
                break;
            default:
                break;
        }
        CGContextFillPath(context);
    }
}

- (BOOL) postData:(NSNumber *)data from:(Processor *)processor
{
    if (inputState <= 2) {
        inputSequence[inputState] = [data integerValue];
    }
    if (inputState == 2) {
        if (inputSequence[inputState] == -1) {
            inputState = 0;
            return YES;
        }
        if ((inputSequence[0] >= 0) && (inputSequence[0] < VIDEOWIDTH) && (inputSequence[1] >= 0) && (inputSequence[1] < VIDEOHEIGHT)) {
            bytes[inputSequence[0]][inputSequence[1]] = inputSequence[2]%5;
            [self setNeedsDisplay];
        }
    } else {
        inputState++;
    }
    return YES;
}

@end

@implementation VideoBus

- (BOOL)postData:(NSNumber *)data from:(Processor *)processor
{
    if (data) {
        return [self.adapter postData:data from:processor];
    }
    return NO;
}

- (NSString *)defaultDownName
{
    return @"▼ VIDEO";
}

- (void)reset
{
    [self.adapter reset];
}
@end
