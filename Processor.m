//
//  Processor.m
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "Processor.h"
#import "Bus.h"
#import "FontHelper.h"
#define InstructionLimit 16
//#define InfoWidth 60
//#define StatusHeight 20

typedef enum : NSUInteger {
    iEmpty = 0,
    iNOP = 1,
    iJMP = 2,
    iJEZ = 3,
    iJNZ = 4,
    iJLZ = 5,
    iJGZ = 6,
    iJRO = 7,
    iMOV = 8,
    iADD = 9,
    iSUB = 10,
    iNEG = 11,
    iSAV = 12,
    iSWP = 13
} InstructionType;

typedef enum : NSUInteger {
    aUnknown = 0,
    aDirection = 1<<0, // + LAST, ANY
    aNumber = 1<<1,
    aLabel = 1<<2,
    aRegister = 1<<3
} ArgumentType;

@class Instruction;

@interface Argument : NSObject
@property (nonatomic) ArgumentType type;
@property (nonatomic, strong) id value;
@property (nonatomic, strong) Instruction *target;
@property (nonatomic) BOOL forOutput;
+ (instancetype)argumentWithString:(NSString *)string allowOnly:(NSInteger)argumentMask;
@end

@interface Instruction : NSObject
@property (nonatomic, assign) InstructionType type;
@property (nonatomic, strong) NSString *labelName;
@property (nonatomic, strong) Argument *arg0;
@property (nonatomic, strong) Argument *arg1;
+ (instancetype)instructionWithString:(NSString *)string errorMessage:(NSString ** _Nonnull) errorMessage errorIndex:(NSInteger * _Nonnull)errorIndex;
@end

@interface LabelInstruction : Instruction
@end

@interface Processor () {
    CGFloat lastK;
    NSInteger stack[InstructionLimit];
    BOOL justPicked;
    NSInteger stackPtr;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, weak) UITextView *editView;
@property (nonatomic, weak) UITextView *stackView;
@property (nonatomic, weak) UITextView *infoView;
@property (nonatomic, weak) UILabel *statusLabel;
@property (nonatomic, assign, setter = setInstructionPointer:) NSInteger ip;
@property (nonatomic, strong, setter = setAcc:) NSNumber *acc;
@property (nonatomic, strong, setter = setBak:) NSNumber *bak;
@property (nonatomic, strong, setter = setLast:) Bus *last;
@property (nonatomic, strong) NSMutableArray *program;
@property (nonatomic, strong) NSString *lastCompilationError;
@property (nonatomic) NSInteger lastCompilationErrorLine;
@property (nonatomic) NSInteger lastCompilationErrorColumn;
@property (nonatomic, assign) NSInteger stepCount;
@property (nonatomic, assign, setter = setStepsWorked:) NSInteger stepsWorked;
@property (nonatomic, assign) BOOL hasProgramUpdates;
@property (nonatomic, assign) BOOL hasAnyVisualAttributes;
@property (nonatomic, weak) UIButton *modeButton;

@end

@implementation Processor

static NSDictionary *commands;

- (instancetype)initWithName:(NSString *)name
{
    if (self = [super init]) {
        static dispatch_once_t pred;
        dispatch_once(&pred, ^{
            NSMutableDictionary *commandsDict = [NSMutableDictionary dictionary];
            commandsDict[@"NOP"] = @{@"Idx": @(iNOP), @"Description": @"Empty operation"};
            commandsDict[@"JMP"] = @{@"Idx": @(iJMP), @"Description": @"Jump to a given\n#label or\n#integer offset"};
            commandsDict[@"JEZ"] = @{@"Idx": @(iJEZ), @"Description": @"Jump when acc\n#equals zero"};
            commandsDict[@"JNZ"] = @{@"Idx": @(iJNZ), @"Description": @"Jump when acc\n#not equals zero"};
            commandsDict[@"JLZ"] = @{@"Idx": @(iJLZ), @"Description": @"Jump when acc\n#is less than zero"};
            commandsDict[@"JGZ"] = @{@"Idx": @(iJGZ), @"Description": @"Jump when acc\n#is more than zero"};
            commandsDict[@"JRO"] = @{@"Idx": @(iJRO), @"Description": @"Jump by given offset"};
            commandsDict[@"MOV"] = @{@"Idx": @(iMOV), @"Description": @"Move value from\n#first agrument\n#into second"};
            commandsDict[@"ADD"] = @{@"Idx": @(iADD), @"Description": @"Add value to acc"};
            commandsDict[@"SUB"] = @{@"Idx": @(iSUB), @"Description": @"Subtract value\n#from acc"};
            commandsDict[@"NEG"] = @{@"Idx": @(iNEG), @"Description": @"Negate acc"};
            commandsDict[@"SAV"] = @{@"Idx": @(iSAV), @"Description": @"Save acc value\n#to bak"};
            commandsDict[@"SWP"] = @{@"Idx": @(iSWP), @"Description": @"Swap acc and bak"};
            commands = [commandsDict copy];
        });
        self.name = name;
    }
    return self;
}

+ (NSDictionary *)commandsDict
{
    return commands;
}

- (NSNumber *) pickable
{
    if ((self.mode == CoreModeStack) && !justPicked && (stackPtr > 0)) {
        return @(stack[stackPtr - 1]);
    }
    return nil;
}

- (NSNumber *) pick
{
    if ((self.mode == CoreModeStack) && !justPicked && (stackPtr > 0)) {
        justPicked = YES;
        NSNumber *result = @(stack[--stackPtr]);
        [self updateInfoView];
        return result;
    }
    return nil;
}

- (void) push:(NSNumber *)data
{
    if (data == nil) return;
    if ((self.mode == CoreModeStack) && !justPicked) {
        justPicked = YES;
        if (stackPtr == InstructionLimit) {
            stack[stackPtr - 1] = [data integerValue];
        } else {
            stack[stackPtr++] = [data integerValue];
        }
        [self updateInfoView];
    }
}

- (void) stepIsOver
{
    justPicked = NO;
}

- (CGRect)editViewRectFromSize:(CGSize)size
{
    return CGRectInset(CGRectMake(0, 0, size.width*160.0/220.0 + 1, size.height*200.0/220.0 + 1), 1, 1);
}

- (CGRect)modeButtonRectFromSize:(CGSize)size
{
    return CGRectInset(CGRectMake(size.width*160.0/220.0, 0, size.width*60.0/220.0, size.height * 40.0/220.0 + 1), 1, 1);
}

- (CGRect)infoViewRectFromSize:(CGSize)size
{
    return CGRectInset(CGRectMake(size.width*160.0/220.0, 0, size.width*60.0/220.0, size.height*200.0/220.0 + 1), 1, 1);
}

- (CGRect)statusRectFromSize:(CGSize)size
{
    return CGRectInset(CGRectMake(0, size.height*200.0/220.0, size.width, size.height*20.0/220.0), 1, 1);
}

- (void)setMode:(CoreMode)mode
{
    if (_mode == mode) return;
    _mode = mode;
    if (mode == CoreModeProcessor) {
        self.editView.hidden = NO;
        self.stackView.hidden = YES;
        [self compile];
        [self reset];
    } else {
        self.editView.hidden = YES;
        self.stackView.hidden = NO;
    }
    [self updateInfoView];
}

- (void) setView:(UIView *)view
{
    self.hasProgramUpdates = YES;
    lastK = 1.0;
    _view = view;
    _acc = @0;
    _bak = @0;
    if (view == nil) return;
    view.backgroundColor = [UIColor whiteColor];
    CGSize size = view.bounds.size;
    UITextView *editView = [[UITextView alloc] initWithFrame:[self editViewRectFromSize:size]];
    editView.backgroundColor = [UIColor blackColor];
    editView.font = [FontHelper defaultFont];
    editView.textColor = [UIColor grayColor];
    editView.delegate = self;
    editView.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    editView.autocorrectionType = UITextAutocorrectionTypeNo;
    [view addSubview:editView];
    self.editView = editView;
    UITextView *stackView = [[UITextView alloc] initWithFrame:[self editViewRectFromSize:size]];
    stackView.backgroundColor = [UIColor blackColor];
    stackView.font = [FontHelper defaultFont];
    stackView.textColor = [UIColor lightGrayColor];
    stackView.editable = NO;
    stackView.selectable = NO;
    stackView.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    stackView.autocorrectionType = UITextAutocorrectionTypeNo;
    stackView.hidden = YES;
    [view addSubview:stackView];
    self.stackView = stackView;
    UITextView *infoView = [[UITextView alloc] initWithFrame:[self infoViewRectFromSize:size]];
    infoView.backgroundColor = [UIColor blackColor];
    infoView.font = [FontHelper defaultFont];
    infoView.textColor = [UIColor grayColor];
    infoView.editable = NO;
    infoView.textAlignment = NSTextAlignmentCenter;
    [infoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoTapped:)]];
    infoView.userInteractionEnabled = YES;
    infoView.selectable = NO;
    self.infoView = infoView;
    UIButton *modeButton = [[UIButton alloc] initWithFrame:[self modeButtonRectFromSize:size]];
    modeButton.backgroundColor = [UIColor blackColor];
    modeButton.titleLabel.font = [FontHelper defaultFont];
    modeButton.titleLabel.textColor = [UIColor whiteColor];
    [modeButton setTitle:@"MODE" forState:UIControlStateNormal];
    [modeButton addTarget:self action:@selector(modeTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.modeButton = modeButton;
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:[self statusRectFromSize:size]];
    self.statusLabel = statusLabel;
    self.statusLabel.backgroundColor = [UIColor blackColor];
    self.statusLabel.font = [FontHelper defaultFont];
    self.statusLabel.textColor = [UIColor redColor];
    [self.statusLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoTapped:)]];
    self.statusLabel.userInteractionEnabled = YES;
    [view addSubview:self.statusLabel];
    [self updateInfoView];
    [view addSubview:infoView];
    [view addSubview:modeButton];
}

- (void)rebuildFramesWithK:(CGFloat)k
{
    lastK = k;
    CGSize size = self.view.bounds.size;
    [self.editView setFrame:[self editViewRectFromSize:size]];
    [self.stackView setFrame:[self editViewRectFromSize:size]];
    [self.infoView setFrame:[self infoViewRectFromSize:size]];
    [self.modeButton setFrame:[self modeButtonRectFromSize:size]];
    [self.statusLabel setFrame:[self statusRectFromSize:size]];
    self.editView.font = [FontHelper defaultFontDecreasedBy:k];
    self.stackView.font = [FontHelper defaultFontDecreasedBy:k];
    self.infoView.font = [FontHelper defaultFontDecreasedBy:k];
    self.modeButton.titleLabel.font = [FontHelper defaultFontDecreasedBy:k];
    self.statusLabel.font = [FontHelper defaultFontDecreasedBy:k];
}

- (void)infoTapped:(UIView *)sender
{
    [self.delegate endEditing];
//    [self.editView endEditing:NO];
}

- (void)modeTapped:(UIButton *)mode
{
    [self.delegate endEditing];
    if ([self.delegate running]) return;
    [[[UIAlertView alloc] initWithTitle:@"Switch Mode"
                                message:nil
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Processor", @"Stack"/*, @"LUT"*/, nil] show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            self.mode = CoreModeProcessor;
            break;
        case 2:
            self.mode = CoreModeStack;
            self.statusLabel.text = @"";
            break;
        case 3:
            self.mode = CoreModeLUT;
            self.statusLabel.text = @"";
            break;
        default:
            return;
    }
    [self reset];
}

- (void)compile
{
    if (self.mode != CoreModeProcessor) return;
    NSString *text = self.editView.text;
    __block NSInteger errorLine = 0;
    __block NSInteger errorColumn = 0;
    __block NSString *errorMessage = nil;
    NSMutableArray *program = [NSMutableArray array];
    [text enumerateLinesUsingBlock:^(NSString * _Nonnull line, BOOL * _Nonnull stop) {
        Instruction *instruction = [Instruction instructionWithString:line errorMessage:&errorMessage errorIndex:&errorColumn];
        if (instruction) {
            [program addObject:instruction];
        } else {
            *stop = YES;
        }
    }];
    if (errorMessage == nil) {
        if (program.count > InstructionLimit) {
            errorMessage = @"Instruction limit reached";
        } else {
            [program enumerateObjectsUsingBlock:^(Instruction *_Nonnull instruction, NSUInteger idx, BOOL * _Nonnull stop) {
                if (instruction.arg0 && (instruction.arg0.type == aLabel)) {
                    instruction.arg0.target = nil;
                    NSString *value = [instruction.arg0.value uppercaseString];
                    for (Instruction *instr in program) {
                        if ([[instr.labelName uppercaseString] isEqualToString:value]) {
                            instruction.arg0.target = instr;
                            break;
                        }
                    }
                    if (instruction.arg0.target == nil) {
                        errorMessage = [NSString stringWithFormat:@"Label not found: %@", instruction.arg0.value];
                        errorColumn = 0;
                        errorLine = idx;
                        *stop = YES;
                    }
                }
            }];
        }
    }
    if (errorMessage == nil) {
        self.program = program;
    } else {
        self.program = nil;
    }
    self.lastCompilationError = errorMessage;
    self.lastCompilationErrorColumn = errorColumn;
    self.lastCompilationErrorLine = errorLine;
    self.statusLabel.text = errorMessage;
    [self.delegate processor:self compilationError:errorMessage];
    self.hasProgramUpdates = YES;
}

- (void) updateAttributedText
{
    if ((_ip < 0) && !self.hasAnyVisualAttributes) return;
    NSString *text = self.editView.text;
    if (text == nil) {
        text = [self.editView.attributedText string];
    }
    self.hasAnyVisualAttributes = NO;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@""];
    __block NSInteger i = _ip;
    [text enumerateLinesUsingBlock:^(NSString * _Nonnull line, BOOL * _Nonnull stop) {
        NSDictionary *attributes;
        if (i-- == 0) {
            attributes = @{NSBackgroundColorAttributeName:[UIColor grayColor],
                           NSForegroundColorAttributeName:[UIColor whiteColor],
                           NSFontAttributeName:[FontHelper defaultFontDecreasedBy:lastK]};
            self.hasAnyVisualAttributes = YES;
        } else {
            attributes = @{NSBackgroundColorAttributeName:[UIColor blackColor],
                           NSForegroundColorAttributeName:[UIColor grayColor],
                           NSFontAttributeName:[FontHelper defaultFontDecreasedBy:lastK]};
        }
        [attributedString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", line] attributes:attributes]];
    }];
    self.editView.attributedText = attributedString;
}

- (void) reset
{
    _ip = -1;
    stackPtr = 0;
    self.acc = @0;
    self.bak = @0;
    self.last = nil;
    self.hasProgramUpdates = NO;
    self.stepCount = self.stepsWorked = 0;
    [self updateAttributedText];
}

- (void) setInstructionPointer:(NSInteger)ip
{
    if (ip < 0) {
        ip = 0;
    }
    if (ip >= self.program.count) {
        ip = 0;
    }
    if (ip == _ip) {
        return;
    }
    _ip = ip;
    [self updateAttributedText];
}

- (Bus *)busByName:(NSString *)name out:(BOOL)forOutput
{
    Bus *result = nil;
    if ([name isEqualToString:@"UP"]) {
        result = self.up;
    } else {
        if ([name isEqualToString:@"DOWN"]) {
            result = self.down;
        } else {
            if ([name isEqualToString:@"LEFT"]) {
                result = self.left;
            } else {
                if ([name isEqualToString:@"RIGHT"]) {
                    result = self.right;
                } else {
                    if ([name isEqualToString:@"LAST"]) {
                        result = self.last;
                    } else { // any
                        result = [self busByName:@"UP" out:forOutput];
                        if (result) return result;
                        result = [self busByName:@"DOWN" out:forOutput];
                        if (result) return result;
                        result = [self busByName:@"LEFT" out:forOutput];
                        if (result) return result;
                        return [self busByName:@"RIGHT" out:forOutput];
                    }
                }
            }
        }
    }
    if (forOutput) {
        if ([result readyForOutputFor:self]) return result;
    } else {
        if ([result readyForInputFor:self]) return result;
    }
    return nil;
}

- (void)jumpByInstruction:(Instruction *)instruction
{
    if (instruction.arg0.target) {
        NSInteger index = [self.program indexOfObject:instruction.arg0.target];
        if (index == NSNotFound) return;
        self.ip = index;
    } else {
        self.ip = [instruction.arg0.value integerValue];
    }
}

- (BOOL)canPickValueByArgument:(Argument *)argument
{
    if (argument.type == aDirection) {
        Bus *b0 = [self busByName:argument.value out:argument.forOutput];
        if (b0 == nil) return NO;
    }
    return YES;
}

- (NSNumber *)valueByArgument:(Argument *)argument
{
    if (argument == nil) return nil;
    switch (argument.type) {
        case aDirection: {
            Bus *b0 = [self busByName:argument.value out:argument.forOutput];
            if (b0 == nil) return nil;
            self.last = b0;
            return [b0 getDataFor:self withoutRemoving:NO];
        }
        case aRegister:
            return self.acc;
        case aNumber:
            return argument.value;
        default:
            return nil;
    }
}

- (void) step
{
    if (self.mode == CoreModeProcessor) {
        _stepCount++;
        if (self.program.count == 0) return;
        if (_ip < 0) {
            self.stepsWorked++;
            self.ip = 0;
            return;
        }
        Instruction *instruction = self.program[self.ip];
        switch (instruction.type) {
            case iJMP:
                [self jumpByInstruction:instruction];
                break;
            case iJEZ:
                if ([self.acc integerValue] == 0) {
                    [self jumpByInstruction:instruction];
                } else {
                    self.ip++;
                }
                break;
            case iJNZ:
                if ([self.acc integerValue] != 0) {
                    [self jumpByInstruction:instruction];
                } else {
                    self.ip++;
                }
                break;
            case iJLZ:
                if ([self.acc integerValue] < 0) {
                    [self jumpByInstruction:instruction];
                } else {
                    self.ip++;
                }
                break;
            case iJGZ:
                if ([self.acc integerValue] > 0) {
                    [self jumpByInstruction:instruction];
                } else {
                    self.ip++;
                }
                break;
            case iJRO:
                if ([self canPickValueByArgument:instruction.arg0]) {
                    NSNumber *v0 = [self valueByArgument:instruction.arg0];
                    if (self.ip + [v0 integerValue] >= self.program.count) {
                        self.ip = self.program.count - 1;
                    } else {
                        self.ip += [v0 integerValue];
                    }
                    self.stepsWorked++;
                }
                break;
            case iMOV:
                if ([self canPickValueByArgument:instruction.arg0] && [self canPickValueByArgument:instruction.arg1]) {
                    switch (instruction.arg1.type) {
                        case aDirection: {
                            Bus *b1 = [self busByName:instruction.arg1.value out:instruction.arg1.forOutput];
                            if (b1 == nil) return;
                            self.last = b1;
                            [b1 postData:[self valueByArgument:instruction.arg0]
                                    from:self];
                            break;
                        }
                        case aRegister:
                            self.acc = [self valueByArgument:instruction.arg0];
                            break;
                        default:
                            break;
                    }
                    self.stepsWorked++;
                    self.ip++;
                }
                break;
            case iADD:
                if ([self canPickValueByArgument:instruction.arg0]) {
                    self.acc = @([self.acc integerValue] + [[self valueByArgument:instruction.arg0] integerValue]);
                    self.ip++;
                    self.stepsWorked++;
                }
                break;
            case iSUB:
                if ([self canPickValueByArgument:instruction.arg0]) {
                    self.acc = @([self.acc integerValue] - [[self valueByArgument:instruction.arg0] integerValue]);
                    self.ip++;
                    self.stepsWorked++;
                }
                break;
            case iNEG:
                self.acc = @(-[self.acc integerValue]);
                self.ip++;
                self.stepsWorked++;
                break;
            case iSAV:
                self.bak = self.acc;
                self.ip++;
                self.stepsWorked++;
                break;
            case iSWP: {
                id v = self.acc;
                self.acc = self.bak;
                self.bak = v;
                self.ip++;
                self.stepsWorked++;
                break;
            }
            default:
                self.ip++;
                self.stepsWorked++;
                break;
        }
    }
}

- (void) setAcc:(NSNumber *)acc
{
    _acc = acc;
    [self updateInfoView];
}

- (void) setBak:(NSNumber *)bak
{
    _bak = bak;
    [self updateInfoView];
}

- (void) setLast:(Bus *)last
{
    _last = last;
    [self updateInfoView];
}

- (void) setStepsWorked:(NSInteger)stepsWorked
{
    _stepsWorked = stepsWorked;
    [self updateInfoView];
}

- (void) updateInfoView
{
    if (self.mode == CoreModeProcessor) {
        NSString *idle = [NSString stringWithFormat:@"IDLE\n%@", _stepCount?[NSString stringWithFormat:@"%.0f%%", (_stepCount - _stepsWorked)*100.0/_stepCount]:@"0%"];
        NSNumber *v = [self.last getDataFor:self withoutRemoving:YES];
        [self.infoView setText:[NSString stringWithFormat:@"\n\nACC\n%@\nBAK\n(%@)\nLAST\n\n%@\n%@", _acc, _bak, v?v:@"(N/A)", idle]];
    } else {
        [self.infoView setText:@""];
        NSString *text = [NSString stringWithFormat:@"  **STACK**\n"];
        for (NSInteger i = 0; i < stackPtr; i++) {
            text = [NSString stringWithFormat:@"%@\n      %ld", text, (long)(stack[i])];
        }
        [self.stackView setText:text];
    }
}

- (NSString *)getProgramText
{
    return self.editView.text;
}

- (void) setProgramText:(NSString *)programText
{
    self.editView.text = programText;
    [self compile];
}

#pragma mark UITextViewDelegate methods

//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
//- (BOOL)textViewShouldEndEditing:(UITextView *)textView;
//
//- (void)textViewDidBeginEditing:(UITextView *)textView;
//- (void)textViewDidEndEditing:(UITextView *)textView;
//
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    [self.delegate processorWillChangeProgram:self];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self compile];
}
//
//- (void)textViewDidChangeSelection:(UITextView *)textView;
//
//- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0);
//- (BOOL)textView:(UITextView *)textView shouldInteractWithTextAttachment:(NSTextAttachment *)textAttachment inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0);

@end

@implementation Instruction

+ (instancetype)instructionWithString:(NSString *)string errorMessage:(NSString ** _Nonnull) errorMessage errorIndex:(NSInteger * _Nonnull)errorIndex {
//    if (errorMessage == nil) return nil;
//    if (errorIndex == nil) return nil;
    NSString *labelName = nil;
    NSInteger offset = 0;
    // check for label
    NSRange range = [string rangeOfString:@":"];
    if (range.location != NSNotFound) {
        labelName = [[string substringToIndex:range.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([labelName isEqualToString:@""]) {
            *errorMessage = @"Bad label";
            *errorIndex = range.location;
        }
        offset = range.location + 1;
    }
    string = [string substringFromIndex:offset];
    NSCharacterSet *nonLiterals = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-_#"] invertedSet];
    NSArray *components = [string componentsSeparatedByCharactersInSet:nonLiterals];
    NSInteger state = 0;
    Argument *arg0 = nil;
    Argument *arg1 = nil;
    *errorIndex = offset;
    InstructionType type = iEmpty;
    NSDictionary *command = nil;
    for (NSString *componentString in components) {
        if ([componentString isEqualToString:@""]) continue;
        NSRange r = [componentString rangeOfString:@"#"];
        NSString *component;
        if (r.location != NSNotFound) {
            if (r.location == 0) {
                break;
            }
            component = [[componentString substringToIndex:r.location] uppercaseString];
        } else {
            component = [componentString uppercaseString];
        }
        switch (state) {
            case 0:
                command = commands[component];
                if (command) {
                    type = state = [command[@"Idx"] integerValue];
                } else {
                    state = -1;
                    *errorMessage = [NSString stringWithFormat:@"Unknown operator: %@", component];
                }
                break;
            case iJMP:
            case iJEZ:
            case iJNZ:
            case iJLZ:
            case iJGZ:
                arg0 = [Argument argumentWithString:component allowOnly:aLabel | aNumber];
                if (arg0 == nil) {
                    state = -1;
                    *errorMessage = [NSString stringWithFormat:@"Invalid argument: %@", component];
                } else {
                    state += 100;
                }
                break;
            case iJRO:
            case iMOV:
            case iADD:
            case iSUB:
                arg0 = [Argument argumentWithString:component allowOnly:aDirection | aNumber | aRegister];
                if (arg0 == nil) {
                    state = -1;
                    *errorMessage = [NSString stringWithFormat:@"Invalid argument: %@", component];
                } else {
                    state += 100;
                }
                break;
            case iNOP:
            case iNEG:
            case iSWP:
                state = -1;
                *errorMessage = @"Operand doesn't need arguments";
                break;
            case iJMP + 100:
            case iJEZ + 100:
            case iJNZ + 100:
            case iJLZ + 100:
            case iJGZ + 100:
            case iJRO + 100:
            case iADD + 100:
            case iSUB + 100:
                state = -1;
                *errorMessage = @"Operand doesn't need second argument";
                break;
            case iMOV + 100:
                arg1 = [Argument argumentWithString:component allowOnly:aDirection | aRegister];
                if (arg1 == nil) {
                    state = -1;
                    *errorMessage = [NSString stringWithFormat:@"Invalid argument: %@", component];
                } else {
                    arg1.forOutput = YES;
                    state += 100;
                }
                break;
            default:
                state = -1;
                *errorMessage = @"Operand doesn't need third argument";
                break;
        }
        if (state < 0) {
            return nil;
        }
    }
    switch (state) {
        case iJMP:
        case iJEZ:
        case iJNZ:
        case iJLZ:
        case iJGZ:
            *errorMessage = @"Label expected";
            return nil;
        case iJRO:
        case iMOV:
        case iADD:
        case iSUB:
            *errorMessage = @"Argument expected";
            return nil;
        case iMOV + 100:
            *errorMessage = @"Second argument expected";
            return nil;
        default:
            break;
    }
    Instruction *instruction = [[Instruction alloc] init];
    instruction.labelName = labelName;
    instruction.type = type;
    instruction.arg0 = arg0;
    instruction.arg1 = arg1;
    return instruction;
}

@end
    
@implementation Argument
    
+ (instancetype)argumentWithString:(NSString *)string allowOnly:(NSInteger)argumentMask
{
    if (argumentMask & aDirection) {
        if ([string isEqualToString:@"UP"] || [string isEqualToString:@"DOWN"] || [string isEqualToString:@"LEFT"] || [string isEqualToString:@"RIGHT"] || [string isEqualToString:@"ANY"] || [string isEqualToString:@"LAST"]) {
            Argument *result = [[Argument alloc] init];
            result.type = aDirection;
            result.value = string;
            return result;
        }
    }
    if (argumentMask & aNumber) {
        NSInteger i = [string integerValue];
        if ([string isEqualToString:[NSString stringWithFormat:@"%ld", (long)i]]) {
            Argument *result = [[Argument alloc] init];
            result.type = aNumber;
            result.value = @(i);
            return result;
        }
    }
    if (argumentMask & aRegister) {
        if ([string isEqualToString:@"ACC"] || [string isEqualToString:@"BAK"]) {
            Argument *result = [[Argument alloc] init];
            result.type = aRegister;
            result.value = string;
            return result;
        }
    }
    if (argumentMask & aLabel) {
        Argument *result = [[Argument alloc] init];
        result.type = aLabel;
        result.value = string;
        return result;
    }
    return nil;
}

@end

