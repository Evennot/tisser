//
//  ScenariosViewController.h
//  tisser
//
//  Created by Danila Parkhomenko on 10/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Scenario;
@class ViewController;

@interface ScenarioViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *caption;
@property (weak, nonatomic) IBOutlet UILabel *stats;
- (void)setupFor:(Scenario *)scenario;
@end

@interface SelectedScenarioViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *programName;
@property (weak, nonatomic) IBOutlet UILabel *stats;
@property (weak, nonatomic) IBOutlet UIButton *loadButton;
- (void)setupFor:(Scenario *)scenario;
@end

@interface ScenariosViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) ViewController *master;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIButton *duplicateButton;

@end
