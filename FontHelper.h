//
//  FontHelper.h
//  tisser
//
//  Created by Danila Parkhomenko on 04/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FontHelper : NSObject

+ (UIFont *)defaultFontDecreasedBy:(CGFloat)k;
+ (UIFont *)defaultFont;

@end
