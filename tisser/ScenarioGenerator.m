//
//  ScenarioGenerator.m
//  tisser
//
//  Created by Danila Parkhomenko on 11/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "ScenarioGenerator.h"
#import "Processor.h"
#import <math.h>
#define DATALENGTH 40
#define RNDCNT 10000
@interface ScenarioGenerator () {
    unsigned char pseudoRandomBytes[RNDCNT];
}

@end

@implementation ScenarioGenerator

+ (ScenarioGenerator *)sharedGenerator
{
    static ScenarioGenerator *global = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        global = [[ScenarioGenerator alloc] init];
    });
    return global;
}

- (instancetype)init
{
    if (self = [super init]) {
        double v = 17;
        for (NSInteger i = 0; i < RNDCNT; i++) {
//            v = log(v)*1394807.0;
//            NSInteger x = v;
            pseudoRandomBytes[i] = arc4random_uniform(255);//x;
        }
    }
    return self;
}

- (NSDictionary *)scenarioByIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return [self tutorial];
        case 1:
            return [self sumsub];
        case 2:
            return [self minMax];
        case 3:
            return [self mul3];
        case 4:
            return [self div2];
        case 5:
            return [self mul];
        case 6:
            return [self div];
        case 7:
            return [self sequenceLength];
        case 8:
            return [self sequenceMinMax];
        case 9:
            return [self sequenceMean];
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
            return [self sandbox:index - 9];
        default:
            return nil;
    }
}

- (NSDictionary *)tutorial
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Tutorial";
    __block NSMutableString *commandsString = [@"#Commands list:\n\n" mutableCopy];
    NSDictionary *commandsDict = [Processor commandsDict];
    [commandsDict enumerateKeysAndObjectsUsingBlock:^(NSString *_Nonnull key, NSDictionary *_Nonnull obj, BOOL * _Nonnull stop) {
        [commandsString appendFormat:@"###%@###:\n#%@\n", key, obj[@"Description"]];
    }];
    NSMutableArray *dataArray = [NSMutableArray array];
    for (NSInteger i = 1; i < DATALENGTH; i++) {
        [dataArray addObject:@(i)];
    }
    scenario[@"field"] = @{@"width": @4,
                           @"height": @3,
                           @"Programs":
                               @[
                                   @{
                                       @"i0" :
                                           @{
                                               @"i0": @"#These are processor\n#cores\n"
                                               "#They can perform\n"
                                               "#various assembler\n#commands\n"
                                               "#every processor\n"
                                               "#has two registers\n"
                                               "#ACC and BAK\n"
                                               "#Processors can\n"
                                               "#communicate through\n"
                                               "#data busses\n",
                                               @"i1": commandsString,
                                               @"i2": @"#All processors\n"
                                               "#could pass values\n"
                                               "#between them\n"
                                               "#using LEFT, RIGHT\n"
                                               "#UP, DOWN\n"
                                               "#arguments of the\n"
                                               "#MOV operation"
                                               },
                                       @"i1" :
                                           @{
                                               @"i0": @"#Sample program:\n"
                                               "MOV UP DOWN\n"
                                               "#moves value from\n"
                                               "#input on the top\n"
                                               "#to the processor\n"
                                               "#on the bottom\n",
                                               @"i1": @"MOV UP DOWN\n"
                                               "#Goal is to\n"
                                               "#make output sequence\n"
                                               "#as listed on the\n"
                                               "#right\n",
                                               @"i2": @"MOV UP DOWN"
                                               },
                                       @"i2" :
                                           @{
                                               @"i0": @"MOV UP DOWN\n"
                                               "#tap \"RUN\" to\n"
                                               "#see the outcome",
                                               @"i1": @"MOV UP DOWN",
                                               @"i2": @"MOV UP DOWN"
                                               },
                                       @"i3" :
                                           @{
                                               @"i0": @"#This game was\n"
                                               "#inspired by\n"
                                               "#TIS 100",
                                               @"i1": @"#obviously",
                                               @"i2": @""
                                               },
                                       @"In0": dataArray,
                                       @"In1": dataArray,
                                       @"Out0": dataArray,
                                       @"Out1": dataArray,
                                       }
                                   ]
                           };
    scenario[@"description"] = @"Test setup. \nTest program that takes data from In0 and In1 and writes it into Out0 and Out1";
    return scenario;
}

- (NSDictionary *)emptyFields:(NSInteger) fieldCount ofSizeX:(NSInteger)x Y:(NSInteger)y dataCallback:(NSMutableDictionary *(^)())callback
{
    NSMutableArray *programs = [NSMutableArray array];
    for (NSInteger i = fieldCount; i--; ) {
        NSMutableDictionary *program = callback();
        NSMutableDictionary *column = [NSMutableDictionary dictionary];
        for (NSInteger i = y; i--; ) {
            column[[NSString stringWithFormat:@"i%ld", (long)i]] = @"";
        }
        for (NSInteger i = x; i--; ) {
            program[[NSString stringWithFormat:@"i%ld", (long)i]] = column;
        }
        [programs addObject:program];
    }
    return @{@"width": @(x),
             @"height": @(y),
             @"Programs":programs};
}

- (NSDictionary *)minMax
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Min Max";
    scenario[@"description"] = @"Put min of In0 and In1 to Out0 and max to Out1.";
    __block NSInteger rndIndex = 0;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray1 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        NSMutableArray *dataArray3 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH; i++) {
            NSInteger a = pseudoRandomBytes[rndIndex++]*100.0/255.0;
            NSInteger b = pseudoRandomBytes[rndIndex++]*100.0/255.0;
            [dataArray0 addObject:@(a)];
            [dataArray1 addObject:@(b)];
            [dataArray2 addObject:@(MIN(a, b))];
            [dataArray3 addObject:@(MAX(a, b))];
        }
        return [@{@"In0": dataArray0,
                  @"In1": dataArray1,
                  @"Out0": dataArray2,
                  @"Out1": dataArray3} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)sumsub
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Sum vs Sub";
    scenario[@"description"] = @"Put In0+In1 to Out0 and In0-In1 to Out1.";
    __block NSInteger startIndex = DATALENGTH*6;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray1 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        NSMutableArray *dataArray3 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH; i++) {
            NSInteger a = pseudoRandomBytes[startIndex++]*100.0/255.0;
            NSInteger b = pseudoRandomBytes[startIndex++]*100.0/255.0;
            [dataArray0 addObject:@(a)];
            [dataArray1 addObject:@(b)];
            [dataArray2 addObject:@(a - b)];
            [dataArray3 addObject:@(a + b)];
        }
        return [@{@"In0": dataArray0,
                  @"In1": dataArray1,
                  @"Out0": dataArray2,
                  @"Out1": dataArray3} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)mul3
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"X 3";
    scenario[@"description"] = @"Put In0*3 to Out0";
    __block NSInteger startIndex = DATALENGTH*12;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray1 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        NSMutableArray *dataArray3 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH; i++) {
            NSInteger a = pseudoRandomBytes[startIndex++]*100.0/255.0;
            NSInteger b = pseudoRandomBytes[startIndex++]*100.0/255.0;
            [dataArray0 addObject:@(a)];
            [dataArray1 addObject:@(b)];
            [dataArray2 addObject:@(a - b)];
            [dataArray3 addObject:@(a + b)];
        }
        return [@{@"In0": dataArray0,
                  @"In1": dataArray1,
                  @"Out0": dataArray2,
                  @"Out1": dataArray3} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)mul
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Multiplication";
    scenario[@"description"] = @"Put In0*In1 to Out0";
    __block NSInteger startIndex = DATALENGTH*18;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray1 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH; i++) {
            NSInteger a = pseudoRandomBytes[startIndex++]*25.0/255.0;
            NSInteger b = pseudoRandomBytes[startIndex++]*25.0/255.0;
            [dataArray0 addObject:@(a)];
            [dataArray1 addObject:@(b)];
            [dataArray2 addObject:@(a*b)];
        }
        return [@{@"In0": dataArray0,
                  @"In1": dataArray1,
                  @"Out0": dataArray2,
                  @"Out1": @[]} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)div2
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Division by 2";
    scenario[@"description"] = @"Put In0/2 to Out0. Put remainder to Ou1";
    __block NSInteger startIndex = DATALENGTH*24;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        NSMutableArray *dataArray3 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH; i++) {
            NSInteger a = pseudoRandomBytes[startIndex++]/4;
            NSInteger b;
            [dataArray0 addObject:@(a)];
            if (a & 1) {
                b = 1;
                a--;
            } else {
                b = 0;
            }
            [dataArray2 addObject:@(a/2)];
            [dataArray3 addObject:@(b)];
        }
        return [@{@"In0": dataArray0,
                  @"In1": @[],
                  @"Out0": dataArray2,
                  @"Out1": dataArray3} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)div
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Unsinged division";
    scenario[@"description"] = @"Put In0/In1 to Out0. Put remainder to Ou1";
    __block NSInteger startIndex = DATALENGTH*27;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray1 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        NSMutableArray *dataArray3 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH; i++) {
            NSInteger b = pseudoRandomBytes[startIndex++]/5 + 1;
            NSInteger a = b + (pseudoRandomBytes[startIndex++]/5);
            [dataArray0 addObject:@(a)];
            [dataArray1 addObject:@(b)];
            [dataArray2 addObject:@(a/b)];
            [dataArray3 addObject:@(a%b)];
        }
        return [@{@"In0": dataArray0,
                  @"In1": dataArray1,
                  @"Out0": dataArray2,
                  @"Out1": dataArray3} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)sequenceLength
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Sequence Length";
    scenario[@"description"] = @"Put lengths of zero terminated sequences to Out0";
    __block NSInteger startIndex = DATALENGTH*33;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH;) {
            NSInteger length = pseudoRandomBytes[startIndex++]%10;
            if (i + length >= DATALENGTH) {
                length = DATALENGTH - i;
            }
            [dataArray2 addObject:@(length)];
            while (length--) {
                [dataArray0 addObject:@(pseudoRandomBytes[startIndex++]+1)];
                i++;
            }
            [dataArray0 addObject:@(0)];
            i++;
        }
        return [@{@"In0": dataArray0,
                  @"In1": @[],
                  @"Out0": dataArray2,
                  @"Out1": @[]} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)sequenceMinMax
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Min/Max of a Sequence";
    scenario[@"description"] = @"Put minimum value of a zero terminated sequences to Out0, put maximum value to Out1";
    __block NSInteger startIndex = DATALENGTH*36;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        NSMutableArray *dataArray3 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH;) {
            NSInteger length = pseudoRandomBytes[startIndex++]%10;
            NSInteger min = 400;
            NSInteger max = 0;
            if (i + length >= DATALENGTH) {
                length = DATALENGTH - i;
            }
            while (length--) {
                NSInteger v = pseudoRandomBytes[startIndex++]+1;
                if (v > max) {
                    max = v;
                }
                if (v < min) {
                    min = v;
                }
                [dataArray0 addObject:@(v)];
                i++;
            }
            [dataArray0 addObject:@(0)];
            [dataArray2 addObject:@(min)];
            [dataArray3 addObject:@(max)];
            i++;
        }
        return [@{@"In0": dataArray0,
                  @"In1": @[],
                  @"Out0": dataArray2,
                  @"Out1": dataArray3} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)sequenceMean
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = @"Min/Max of a Sequence";
    scenario[@"description"] = @"Put a mean value of a zero terminated sequences to Out0";
    __block NSInteger startIndex = DATALENGTH*39;
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        NSMutableArray *dataArray0 = [NSMutableArray array];
        NSMutableArray *dataArray2 = [NSMutableArray array];
        for (NSInteger i = 1; i < DATALENGTH;) {
            NSInteger length = pseudoRandomBytes[startIndex++]%10;
            NSInteger l = length;
            NSInteger total = 0;
            if (i + length >= DATALENGTH) {
                length = DATALENGTH - i;
            }
            while (length--) {
                NSInteger v = pseudoRandomBytes[startIndex++]+1;
                total += v;
                [dataArray0 addObject:@(v)];
                i++;
            }
            [dataArray0 addObject:@(0)];
            if (l > 0) {
                [dataArray2 addObject:@(total / l)];
            } else {
                [dataArray2 addObject:@(0)];
            }
            i++;
        }
        return [@{@"In0": dataArray0,
                  @"In1": @[],
                  @"Out0": dataArray2,
                  @"Out1": @[]} mutableCopy];
    }];
    return scenario;
}

- (NSDictionary *)sandbox:(NSInteger)index
{
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    scenario[@"title"] = [NSString stringWithFormat:@"Sandbox %ld", (long)index];
    scenario[@"description"] = @"";
    scenario[@"field"] = [self emptyFields:3 ofSizeX:4 Y:3 dataCallback:^NSMutableDictionary *{
        return [@{@"In0": @[],
                  @"In1": @[],
                  @"Out0": @[],
                  @"Out1": @[]} mutableCopy];
    }];
    return scenario;
}

@end
