//
//  ScenarioGenerator.h
//  tisser
//
//  Created by Danila Parkhomenko on 11/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScenarioGenerator : NSObject

+ (ScenarioGenerator *)sharedGenerator;
- (NSDictionary *)scenarioByIndex:(NSInteger)index;

@end
