//
//  ComputerViewController.h
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class Processor;

typedef enum : NSUInteger {
    StepFailure = 0,
    StepPerformed = 1,
    StepProgramEnded = 2,
} StepResult;

@protocol ComputerProxy
- (StepResult) step:(NSString *_Nonnull*_Nonnull)errorMessage;
- (BOOL) hasProgramUpdates;
- (void) reset;
- (NSMutableDictionary *_Nonnull) programData;
- (BOOL) validateProgram:(NSDictionary *_Nullable)data withError:(NSError * _Nonnull * _Nonnull)error;
- (void) loadFrom:(NSDictionary *_Nullable)data objectives:(NSString *_Nullable)objectives;
- (void) zoomOut;
- (void) zoomIn;
@end

@interface ComputerViewController : UIViewController <ComputerProxy>
@property (strong, nonatomic) IBOutlet UIScrollView *_Nonnull scrollView;


@end
