//
//  Scenario.h
//  tisser
//
//  Created by Danila Parkhomenko on 10/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSArray ProgramPath;

@interface Scenario : NSObject
@property (nonatomic) NSInteger width;
@property (nonatomic) NSInteger height;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) NSString *details;
@property (nonatomic, strong) NSArray *programsArray;

+ (NSArray *)loadWithError:(NSError **) error;
+ (void)saveProgram:(NSDictionary *)program path:(ProgramPath *)path;
+ (ProgramPath *)addUserProgram:(NSDictionary *)program title:(NSString *)title details:(NSString *)details withError:(NSError **)error; // returns Path

@end
