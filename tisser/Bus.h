//
//  Bus.h
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    OutputNotDoneValid = 0,
    OutputNotDoneInvalid = 1,
    OutputDoneValid = 2,
    OutputDoneInvalid = 3,
} BusOutcome;

@class Processor;
@interface Bus : NSObject
@property (nonatomic, strong, setter = setView:) UIView *view;
@property (nonatomic, strong) UILabel *label0;
@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, weak) Processor *left;
@property (nonatomic, weak) Processor *right;
@property (nonatomic, weak) Processor *up;
@property (nonatomic, weak) Processor *down;

- (void)setupViewBetween:(Processor *)upper lower:(Processor *)lower;
- (void)setupViewBetween:(Processor *)left right:(Processor *)right;
- (void)rebuildFramesWithK:(CGFloat)k defaultSpace:(CGFloat)space;
- (BOOL)readyForOutputFor:(Processor *)processor;
- (BOOL)readyForInputFor:(Processor *)processor;
- (NSNumber *) getDataFor:(Processor *)processor withoutRemoving:(BOOL)withoutRemoving;
- (BOOL)postData:(NSNumber *)data from:(Processor *)processor;
- (void)stepIsOver;
- (void)reset;
@end

@interface ExtBus : Bus
@property (nonatomic, strong) NSString *caption;
- (instancetype) initWithCaption:(NSString *)caption;
@end

@interface InputBus : ExtBus

@property (nonatomic) NSInteger index;
@property (nonatomic, strong) NSArray *data;

- (NSAttributedString *)attributedTextWithCaption:(NSString *)caption andK:(CGFloat)k;

@end

@interface OutputBus : ExtBus

@property (nonatomic, strong) NSMutableArray *desiredData;
@property (nonatomic, strong) NSMutableArray *realData;

- (NSAttributedString *)attributedTextWithCaption:(NSString *)caption andK:(CGFloat)k;
- (BusOutcome)ended;

@end