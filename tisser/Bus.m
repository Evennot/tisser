//
//  Bus.m
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "Bus.h"
#import "Processor.h"
#import "FontHelper.h"

@interface Bus () {
    BOOL vertical;
}

@property (nonatomic, strong, setter = setDataLR:) NSNumber *dataLR;
@property (nonatomic, strong, setter = setDataRL:) NSNumber *dataRL;
@property (nonatomic, weak) UILabel *arrow0Label;
@property (nonatomic, weak) UILabel *arrow1Label;
@property (nonatomic) BOOL dataLRJustArrived;
@property (nonatomic) BOOL dataRLJustArrived;
@property (nonatomic) BOOL dataLRJustLeft;
@property (nonatomic) BOOL dataRLJustLeft;
@end

@implementation Bus

- (void)setView:(UIView *)view
{
    _view = view;
}

- (NSString *)defaultDownName
{
    return @"▼";
}

- (void)setupViewBetween:(Processor *)upper lower:(Processor *)lower
{
    self.up = upper;
    self.down = lower;
    upper.down = self;
    lower.up = self;
    CGFloat space = self.view.bounds.size.height;
    CGFloat xStep = self.view.bounds.size.width/8;
    vertical = YES;
    //UILabel *label;
    self.label0 = [[UILabel alloc] initWithFrame:CGRectMake(xStep, 0, xStep, space)];
    self.label0.textColor = [UIColor grayColor];
    self.label0.text = @"";
    self.label0.tag = 1;
    self.label0.font = [FontHelper defaultFont];
    self.label0.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.label0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xStep * 2, 0, (lower && upper)?xStep:xStep*3, space)];
    self.arrow0Label = label;
    label.textColor = [UIColor grayColor];
    label.text = [self defaultDownName];
    [self.view addSubview:label];
    if (lower && upper) {
        self.label1 = [[UILabel alloc] initWithFrame:CGRectMake(xStep * 4, 0, xStep, space)];
        self.label1.textColor = [UIColor grayColor];
        self.label1.text = @"";
        self.label1.tag = 2;
        self.label1.font = [FontHelper defaultFont];
        self.label1.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:self.label1];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xStep * 5, 0, xStep, space)];
        label.textColor = [UIColor grayColor];
        label.text = @"▲";
        self.arrow1Label = label;
        [self.view addSubview:label];
    }
}

- (void)rebuildFramesWithK:(CGFloat)k defaultSpace:(CGFloat)space
{
    if (vertical) {
        UIView *initialView = self.up.view;
        if (initialView) {
            [self.view setFrame:CGRectMake(initialView.frame.origin.x, CGRectGetMaxY(initialView.frame), initialView.frame.size.width, space)];
        } else {
            initialView = self.down.view;
            [self.view setFrame:CGRectMake(initialView.frame.origin.x, CGRectGetMinY(initialView.frame) - space, initialView.frame.size.width, space)];
        }
        CGFloat xStep = self.view.bounds.size.width/8;
        [self.label0 setFrame:CGRectMake(xStep, 0, xStep, space)];
        self.label0.font = [FontHelper defaultFontDecreasedBy:k];
        [self.arrow0Label setFrame:CGRectMake(xStep * 2, 0, xStep, space)];
        if (self.label1) {
            [self.label1 setFrame:CGRectMake(xStep * 4, 0, xStep, space)];
            self.label1.font = [FontHelper defaultFontDecreasedBy:k];
            [self.arrow1Label setFrame:CGRectMake(xStep * 5, 0, xStep, space)];
        }
    } else {
        UIView *initialView = self.left.view;
        if (initialView) {
            [self.view setFrame:CGRectMake(CGRectGetMaxX(initialView.frame), initialView.frame.origin.y, space, initialView.frame.size.height)];
        } else {
            initialView = self.right.view;
            [self.view setFrame:CGRectMake(CGRectGetMinX(initialView.frame) - space, initialView.frame.origin.y, space, initialView.frame.size.height)];
        }
        CGFloat yStep = self.view.bounds.size.height/8;
        [self.label0 setFrame:CGRectMake(0, yStep, space, yStep)];
        self.label0.font = [FontHelper defaultFontDecreasedBy:k];
        [self.arrow0Label setFrame:CGRectMake(0, yStep * 2, space, yStep)];
        [self.label1 setFrame:CGRectMake(0, yStep * 4, space, yStep)];
        self.label1.font = [FontHelper defaultFontDecreasedBy:k];
        [self.arrow1Label setFrame:CGRectMake(0, yStep * 5, space, yStep)];
    }

}

- (void)setDataLR:(NSNumber *)dataLR
{
    _dataLR = dataLR;
    self.label0.text = [NSString stringWithFormat:@"%@", dataLR?dataLR:@""];
}

- (void)setDataRL:(NSNumber *)dataRL
{
    _dataRL = dataRL;
    self.label1.text = [NSString stringWithFormat:@"%@", dataRL?dataRL:@""];
}

- (void)setupViewBetween:(Processor *)left right:(Processor *)right
{
    self.left = left;
    self.right = right;
    left.right = self;
    right.left = self;
    CGFloat space = self.view.bounds.size.width;
    CGFloat yStep = self.view.bounds.size.height/8;
    self.label0 = [[UILabel alloc] initWithFrame:CGRectMake(0, yStep, space, yStep)];
    self.label0.textColor = [UIColor grayColor];
    self.label0.text = @"";
    self.label0.tag = 1;
    self.label0.font = [FontHelper defaultFont];
    self.label0.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.label0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, yStep * 2, space, yStep)];
    label.textColor = [UIColor grayColor];
    label.text = @"▶︎";
    self.arrow0Label = label;
    [self.view addSubview:label];
    self.label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, yStep * 4, space, yStep)];
    self.label1.textColor = [UIColor grayColor];
    self.label1.text = @"";
    self.label1.tag = 2;
    self.label1.font = [FontHelper defaultFont];
    self.label1.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.label1];
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, yStep * 5, space, yStep)];
    label.textColor = [UIColor grayColor];
    label.text = @"◀︎";
    label.textAlignment = NSTextAlignmentRight;
    self.arrow1Label = label;
    [self.view addSubview:label];
}

- (BOOL)readyForOutputFor:(Processor *)processor
{
    if (processor) {
        if ([self.left isEqual:processor] || [self.up isEqual:processor]) {
            Processor *recipient = self.right?self.right:self.down;
            if (recipient && (recipient.mode == CoreModeStack)) {
                return YES;
            }
            return (self.dataLR == nil) && !self.dataLRJustLeft;
        }
    }
    Processor *recipient = self.left?self.left:self.up;
    if (recipient && (recipient.mode == CoreModeStack)) {
        return YES;
    }
    return (self.dataRL == nil) && !self.dataRLJustLeft;
}

- (BOOL)readyForInputFor:(Processor *)processor
{
    if ((processor == nil) || [self.left isEqual:processor] || [self.up isEqual:processor]) {
        Processor *recipient = self.right?self.right:self.down;
        if (recipient && (recipient.mode == CoreModeStack)) {
            return [recipient pickable] != nil;
        }
        if (self.dataRL && !self.dataRLJustArrived) return YES;
    }
    Processor *recipient = self.left?self.left:self.up;
    if (recipient && (recipient.mode == CoreModeStack)) {
        return [recipient pickable] != nil;
    }
    if (self.dataLR && !self.dataLRJustArrived) return YES;
    return NO;
}

- (NSNumber *) getDataFor:(Processor *)processor withoutRemoving:(BOOL)withoutRemoving
{
    if (processor) {
        if ([self.left isEqual:processor] || [self.up isEqual:processor]) {
            Processor *recipient = self.right?self.right:self.down;
            if (recipient && (recipient.mode == CoreModeStack)) {
                if (withoutRemoving) {
                    return [recipient pickable];
                } else {
                    return [recipient pick];
                }
            }
            NSNumber *v = self.dataRL;
            if (!withoutRemoving) {
                self.dataRL = nil;
            }
            return v;
        }
    }
    Processor *recipient = self.left?self.left:self.up;
    if (recipient && (recipient.mode == CoreModeStack)) {
        if (withoutRemoving) {
            return [recipient pickable];
        } else {
            return [recipient pick];
        }
    } else {
        NSNumber *v = self.dataLR;
        if (!withoutRemoving) {
            self.dataLR = nil;
        }
        return v;
    }
}

- (BOOL)postData:(NSNumber *)data from:(Processor *)processor
{
    if ((processor == nil) || [self.left isEqual:processor] || [self.up isEqual:processor]) {
        Processor *recipient = self.right?self.right:self.down;
        if (recipient && (recipient.mode == CoreModeStack)) {
            [recipient push:data];
        } else {
            //NSLog(@"%@ tries to push %@ to a bus to %@", processor?[NSString stringWithFormat:@"Core %@", processor.name]:@"Input", data, self.right?self.right.name:(self.down?self.down.name:@"Output"));
            if (self.dataLR) return NO;
            //NSLog(@"... successfully");
            self.dataLR = data;
            self.dataLRJustArrived = YES;
        }
        return YES;
    }
    Processor *recipient = self.left?self.left:self.up;
    if (recipient && (recipient.mode == CoreModeStack)) {
        [processor push:data];
    } else {
        if ([self.right isEqual:processor] || [self.down isEqual:processor]) {
            //NSLog(@"Core %@ tries to push %@ to a bus to %@", processor.name, data, self.left?self.left.name:self.up.name);
            if (self.dataRL) return NO;
            //NSLog(@"... successfully");
            self.dataRL = data;
            self.dataRLJustArrived = YES;
        }
    }
    return YES;
}

- (void)stepIsOver
{
    self.dataLRJustArrived = self.dataRLJustArrived = self.dataLRJustLeft = self.dataRLJustLeft = NO;
}

- (void)reset
{
    [self stepIsOver];
    self.dataLR = self.dataRL = nil;
}

@end

@implementation InputBus

- (void) setData:(NSMutableArray *)data {
    self.dataLR = [data firstObject];
    _data = data;
}

- (BOOL)readyForOutputFor:(Processor *)processor {
    return NO;
}

- (BOOL)readyForInputFor:(Processor *)processor {
    return self.index < self.data.count;
}

- (NSNumber *) getDataFor:(Processor *)processor withoutRemoving:(BOOL)withoutRemoving {
    if (self.index < self.data.count) {
        self.dataLR = self.data[self.index];
        if (!withoutRemoving) {
            //NSLog(@"Core %@ gets %@ from the input", processor.name, self.dataLR);
            self.index++;
        }
        return self.dataLR;
    }
    return nil;
}

- (BOOL)postData:(NSNumber *)data from:(Processor *)processor {
    return NO;
}

- (NSAttributedString *) attributedTextWithCaption:(NSString *)caption andK:(CGFloat)k
{
    NSDictionary *blackText = @{NSBackgroundColorAttributeName:[UIColor blackColor],
                                NSForegroundColorAttributeName:[UIColor grayColor],
                                NSFontAttributeName:[FontHelper defaultFontDecreasedBy:k]};
    NSString *beginning = [NSString stringWithFormat:@"%@\n\n", caption];
    for (NSInteger i = 0; i < MIN(self.index, self.data.count); i++) {
        beginning = [NSString stringWithFormat:@"%@%@\n", beginning, self.data[i]];
    }
    if ((self.index >= 0) && (self.index < self.data.count)) {
        NSString *highlighted = [NSString stringWithFormat:@"%@\n", self.data[self.index]];
        NSString *ending = nil;
        for (NSInteger i = self.index + 1; i < self.data.count; i++) {
            ending = [NSString stringWithFormat:@"%@%@\n", ending?ending:@"", self.data[i]];
        }
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:beginning
                                                                                             attributes:blackText];
        [attributedString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:highlighted attributes:@{NSBackgroundColorAttributeName:[UIColor grayColor], NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[FontHelper defaultFontDecreasedBy:k]}]];
        if (ending) {
            [attributedString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:ending attributes:blackText]];
        }
        return attributedString;
    }
    for (NSInteger i = MIN(self.index, self.data.count) + 1; i < self.data.count; i++) {
        beginning = [NSString stringWithFormat:@"%@%@\n", beginning, self.data[i]];
    }
    return [[NSMutableAttributedString alloc] initWithString:beginning attributes:blackText];
}

@end

@implementation OutputBus

- (BOOL)readyForOutputFor:(Processor *)processor {
    return YES;
}

- (BOOL)readyForInputFor:(Processor *)processor {
    return NO;
}

- (NSNumber *) getDataFor:(Processor *)processor withoutRemoving:(BOOL)withoutRemoving {
    return nil;
}

- (BOOL)postData:(NSNumber *)data from:(Processor *)processor {
    if (data) {
        if (self.realData == nil) {
            self.realData = [NSMutableArray array];
        }
        //NSLog(@"Core %@ outputs %@", processor.name, data);
        [self.realData addObject:data];
        self.dataLR = data;
        return YES;
    }
    return NO;
}

- (NSAttributedString *) attributedTextWithCaption:(NSString *)caption andK:(CGFloat)k
{
    NSDictionary *whiteText = @{NSBackgroundColorAttributeName:[UIColor blackColor],
                                NSForegroundColorAttributeName:[UIColor whiteColor],
                                NSFontAttributeName:[FontHelper defaultFontDecreasedBy:k]};
    NSDictionary *grayText = @{NSBackgroundColorAttributeName:[UIColor blackColor],
                               NSForegroundColorAttributeName:[UIColor grayColor],
                               NSFontAttributeName:[FontHelper defaultFontDecreasedBy:k]};
    NSDictionary *redText = @{NSBackgroundColorAttributeName:[UIColor blackColor],
                              NSForegroundColorAttributeName:[UIColor redColor],
                              NSFontAttributeName:[FontHelper defaultFontDecreasedBy:k]};
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", caption] attributes:grayText];
    for (NSInteger i = 0; true; i++) {
        NSAttributedString *addition;
        if (self.realData.count > i) {
            if (self.desiredData.count > i) {
                addition = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", self.realData[i]]
                                                                  attributes:[self.desiredData[i] isEqualToNumber:self.realData[i]]?whiteText:redText];
            } else {
                addition = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", self.realData[i]]
                                                                  attributes:redText];
            }
        } else {
            if (self.desiredData.count > i) {
                addition = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", self.desiredData[i]]
                                                                  attributes:grayText];
            } else {
                break;
            }
        }
        [attributedString appendAttributedString:addition];
    }
    return attributedString;
}

- (BusOutcome)ended
{
    if (self.realData.count == self.desiredData.count) {
        for (NSInteger i = 0; i < self.realData.count; i++) {
            if (![self.desiredData[i] isEqualToNumber:self.realData[i]]) {
                return OutputDoneInvalid;
            }
        }
        return OutputDoneValid;
    }
    for (NSInteger i = 0; i < self.realData.count; i++) {
        if (![self.desiredData[i] isEqualToNumber:self.realData[i]]) {
            return OutputNotDoneInvalid;
        }
    }
    return OutputNotDoneValid;
}

@end

@implementation ExtBus

- (instancetype) initWithCaption:(NSString *)caption
{
    if (self = [super init]) {
        self.caption = caption;
    }
    return self;
}

- (NSString *)defaultDownName
{
    return [NSString stringWithFormat:@"%@%@", [super defaultDownName], self.caption];
}

@end