//
//  main.m
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
