//
//  ViewController.h
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComputerViewController.h"
#import "Scenario.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *additionalView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *stepButton;
@property (weak, nonatomic) IBOutlet UIButton *runButton;
@property (nonatomic, weak) id<ComputerProxy> computerProxy;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;

- (BOOL)validateProgram:(NSDictionary *_Nullable)data withError:(NSError * _Nonnull * _Nullable)error;

- (void)loadProgram:(NSDictionary *_Nullable)program title:(NSString * _Nullable)title details:(NSString * _Nullable)details path:(ProgramPath * _Nonnull)path;

@end

