//
//  ViewController.m
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "ViewController.h"
#import "ScenariosViewController.h"
#import "Scenario.h"

@interface ViewController () {
    BOOL running;
    BOOL fastForward;
    BOOL firstProgramLoaded;
}

@property (nonatomic, assign) NSInteger totalStepCount;
@property (nonatomic, strong) NSArray *path;
@property (nonatomic, strong) NSString *stats;
@property (nonatomic, strong) NSString *details;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.statusLabel.text = @"";
    firstProgramLoaded = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (firstProgramLoaded) return;
    NSError *error = nil;
    NSArray *scenarios = [Scenario loadWithError:&error];
#warning LOAD LAST PROGRAM
    [scenarios enumerateObjectsUsingBlock:^(Scenario *_Nonnull scenario, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([scenario.caption isEqualToString:@"Tutorial"]) {
            if (scenario.programsArray.count > 0) {
                [self loadProgram:[scenario.programsArray firstObject]
                            title:scenario.caption
                          details:scenario.details
                             path:@[@(idx), @0]];
                *stop = YES;
                firstProgramLoaded = YES;
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)step {
    NSString *message = nil;
    _totalStepCount++;
    if ([self.computerProxy step:&message] == StepProgramEnded) {
        self.stats = [NSString stringWithFormat:@"%@. Step count: %ld", message, (long)_totalStepCount];
        [self runTapped:nil];
        [[[UIAlertView alloc] initWithTitle:@"Program Ended"
                                    message:self.stats
                                   delegate:nil
                          cancelButtonTitle:@"Done"
                          otherButtonTitles:nil] show];
        _totalStepCount = 0;
        return YES;
    }
    self.statusLabel.text = message;
    return NO;
}

- (IBAction)stepTapped:(id)sender {
    if (running) {
        fastForward = !fastForward;
    } else {
        if ([self.computerProxy hasProgramUpdates] || [self step]) {
            [self.computerProxy reset];
            self.totalStepCount = 0;
        }
    }
}

- (void)internalRunLoop {
    if (!running) return;
    if ([self.computerProxy hasProgramUpdates] || [self step]) {
        [self.computerProxy reset];
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((fastForward?0.0001:0.05) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self internalRunLoop];
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showScenarios"]) {
        ((ScenariosViewController *)segue.destinationViewController).master = self;
    }
}

- (IBAction)runTapped:(id)sender {
    if (running) {
        running = NO;
        [self.runButton setTitle:@"RUN" forState:UIControlStateNormal];
        [self.stepButton setTitle:@"STEP" forState:UIControlStateNormal];
        //self.stepButton.enabled = YES;
    } else {
        if ([self.computerProxy hasProgramUpdates]) {
            [self.computerProxy reset];
        }
        running = YES;
        fastForward = NO;
        [self.runButton setTitle:@"STOP" forState:UIControlStateNormal];
        [self.stepButton setTitle:@"FFWD" forState:UIControlStateNormal];
        //self.stepButton.enabled = NO;
        [self internalRunLoop];
    }
}

- (IBAction)saveTapped:(id)sender {
    NSMutableDictionary *program = [self.computerProxy programData];
    if (self.stats) {
        program[@"stats"] = self.stats;
    }
    [Scenario saveProgram:program path:self.path];
}

- (IBAction)loadTapped:(id)sender {
    [self performSegueWithIdentifier:@"showScenarios" sender:nil];
    return;
    NSURL *fileURL = [NSURL URLWithString:@"temp_program"
                            relativeToURL:[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil]];
    NSData *data = [NSData dataWithContentsOfURL:fileURL];
    if (data) {
    }
}

- (IBAction)zoominTapped:(id)sender {
    [self.computerProxy zoomIn];
}

- (IBAction)zoomoutTapped:(id)sender {
    [self.computerProxy zoomOut];
}

- (void)loadProgram:(NSDictionary *)program title:(NSString *)title details:(NSString *)details path:(ProgramPath *)path
{
    self.stats = program[@"stats"];
    self.details = details;
    if (path.count == 0) { // user program
        NSError *error = nil;
        path = [Scenario addUserProgram:program title:title details:details withError:&error];
        if (path == nil) {
            [[[UIAlertView alloc] initWithTitle:@"Failed to Load Scenario"
                                        message:error.localizedDescription
                                       delegate:nil
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:nil] show];
            return;
        }
    }
    self.path = path;
    [self.computerProxy loadFrom:program objectives:details];
}

- (BOOL) validateProgram:(NSDictionary *)data withError:(NSError **)error;
{
    return [self.computerProxy validateProgram:data withError:error];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
#warning VIDEO
    }
}

- (IBAction)strangeButtonTapped:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Mission"
                                message:self.details
                               delegate:self
                      cancelButtonTitle:@"Done"
                      otherButtonTitles:/*@"Start Video",*/nil] show];
}

@end
