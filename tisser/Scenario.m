//
//  Scenario.m
//  tisser
//
//  Created by Danila Parkhomenko on 10/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "Scenario.h"
#import "Processor.h"
#import "ScenarioGenerator.h"

@implementation Scenario

+ (NSArray *)loadDictionariesWithError:(NSError **)error
{
    NSURL *fileURL = [NSURL URLWithString:@"scenarios1.json"
                            relativeToURL:[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil]];
    NSData *data = [NSData dataWithContentsOfURL:fileURL];
    if (data == nil) {
        NSMutableArray *scenarios = [NSMutableArray array];
        for (NSInteger i = 0; true; i++) {
            NSDictionary *scenario = [[ScenarioGenerator sharedGenerator] scenarioByIndex:i];
            if (scenario) {
                [scenarios addObject:scenario];
            } else {
                break;
            }
        }
        data = [NSJSONSerialization dataWithJSONObject:scenarios options:0 error:error];
#warning write scenarios to disc
        if (*error) {
            return nil;
        }
    }
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
}

+ (NSArray *)loadWithError:(NSError **) error
{
    return [self parseArray:[self loadDictionariesWithError:error]];
}

+ (NSArray *)parseArray:(NSArray *)array
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:array.count];
    for (NSDictionary *dict in array) {
        Scenario *scenario = [[Scenario alloc] init];
        scenario.caption = dict[@"title"];
        scenario.details = dict[@"description"];
        NSDictionary *field = dict[@"field"];
        scenario.width = [field[@"width"] integerValue];
        scenario.height = [field[@"height"] integerValue];
        scenario.programsArray = field[@"Programs"];
        [result addObject:scenario];
    }
    return [result copy];
}

+ (void)saveProgram:(NSDictionary *)program path:(ProgramPath *)path
{
    if ((path.count <= 1) || (program == nil)) return;
    NSInteger scenarioIndex = [[path firstObject] integerValue];
    NSError *error = nil;
    NSArray *scenarios = [self loadDictionariesWithError:&error];
    if (scenarios.count > scenarioIndex) {
        NSMutableArray *mutableScenarios = [scenarios mutableCopy];
        NSMutableDictionary *scenario = [mutableScenarios[scenarioIndex] mutableCopy];
        [mutableScenarios replaceObjectAtIndex:scenarioIndex withObject:scenario];
        NSMutableDictionary *field = [scenario[@"field"] mutableCopy];
        scenario[@"field"] = field;
        NSMutableArray *programs = [field[@"Programs"] mutableCopy];
        field[@"Programs"] = programs;
        NSInteger programIndex = [path[1] integerValue];
        if (programs.count > programIndex) {
            [programs replaceObjectAtIndex:programIndex withObject:program];
            NSURL *fileURL = [NSURL URLWithString:@"scenarios.json"
                                    relativeToURL:[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil]];
            NSData *data = [NSJSONSerialization dataWithJSONObject:mutableScenarios options:NSJSONWritingPrettyPrinted error:&error];
            [data writeToURL:fileURL atomically:YES];
            NSLog(@"saved to %@", fileURL);
        }
    }
}

+ (ProgramPath *)addUserProgram:(NSDictionary *)program title:(NSString *)title details:(NSString *)details withError:(NSError *__autoreleasing *)error
{
    if (program == nil) return nil;
    NSArray *scenarios = [self loadDictionariesWithError:error];
    if (scenarios == nil) return nil;
    NSMutableArray *mutableScenarios = [scenarios mutableCopy];
    NSInteger scenarioIndex = mutableScenarios.count;
    NSMutableDictionary *scenario = [NSMutableDictionary dictionary];
    [mutableScenarios addObject:scenario];
    NSMutableDictionary *field = [NSMutableDictionary dictionary];
    scenario[@"field"] = field;
    scenario[@"title"] = title?title:@"User scenario";
    scenario[@"description"] = details?details:@"";
    field[@"width"] = @4;
    field[@"height"] = @3;
    NSMutableArray *programs = [NSMutableArray array];
    field[@"Programs"] = programs;
    [programs addObject:program];
    NSURL *fileURL = [NSURL URLWithString:@"scenarios.json"
                            relativeToURL:[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil]];
    NSData *data = [NSJSONSerialization dataWithJSONObject:mutableScenarios options:NSJSONWritingPrettyPrinted error:error];
#warning remove copypasta^^
    if (data && [data writeToURL:fileURL atomically:YES]) {
        return @[@(scenarioIndex), @0];
    }
    return nil;
}

@end
