//
//  ComputerViewController.m
//  tisser
//
//  Created by Danila Parkhomenko on 03/10/15.
//  Copyright © 2015 Itheme. All rights reserved.
//

#import "ComputerViewController.h"
#import "Processor.h"
#import "Bus.h"
#import "ViewController.h"
#import "IO.h"
#import "Video.h"
#import "FontHelper.h"

#define XCORECOUNT 4
#define YCORECOUNT 3
#define VideoWidth 300
#define ProcessorWidth 220
#define IOWidth 220
#define ProcessorHeight 220
#define Margin 44

@class VideoBus;

@interface ComputerViewController () <ProcessorDelegate>

@property (nonatomic, strong) NSArray *columns;
@property (nonatomic, strong) NSArray *busses;
@property (nonatomic, strong) InputBus *input0;
@property (nonatomic, strong) InputBus *input1;
@property (nonatomic, strong) OutputBus *output0;
@property (nonatomic, strong) OutputBus *output1;
@property (nonatomic, strong) VideoBus *videoOut;
@property (nonatomic, strong) UITextView *objectives;
@property (nonatomic, strong) IO *io;

@property (nonatomic, strong) NSArray *inData0;
@property (nonatomic, strong) NSArray *inData1;
@property (nonatomic, strong) NSArray *outData0;
@property (nonatomic, strong) NSArray *outData1;

@property (nonatomic, assign) NSInteger zoom;

@end

@implementation ComputerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.zoom = 0;
    
    [self.scrollView setContentSize:[self scrollViewContentSize]];
    self.scrollView.backgroundColor = [UIColor blackColor];
    NSMutableArray *busses = [NSMutableArray array];
    NSMutableArray *columnsMutable = [NSMutableArray array];
    for (NSInteger i = 0; i < XCORECOUNT; i++) {
        NSMutableArray *column = [NSMutableArray array];
        for (NSInteger j = 0; j < YCORECOUNT; j++) {
            Processor *processor = [[Processor alloc] initWithName:[NSString stringWithFormat:@"%ld.%ld", (long)i, (long)j]];
            processor.delegate = self;
            UIView *view = [[UIView alloc] initWithFrame:[self processorFrameForX:i Y:j]];
            processor.view = view;
            [self.scrollView addSubview:view];
            [column addObject:processor];
        }
        [columnsMutable addObject:[column copy]];
    }
    self.columns = [columnsMutable copy];
    for (NSInteger i = 0; i < XCORECOUNT - 1; i++) {
        for (NSInteger j = 0; j < YCORECOUNT; j++) {
            Bus *bus = [[Bus alloc] init];
            CGRect frame = CGRectMake((i + 1)*(Margin + ProcessorWidth), (j + 1)*Margin + (j * ProcessorHeight), Margin, ProcessorHeight);
            UIView *view = [[UIView alloc] initWithFrame:frame];
            bus.view = view;
            [bus setupViewBetween:self.columns[i][j] right:self.columns[i + 1][j]];
            [busses addObject:bus];
            [self.scrollView addSubview:view];
        }
    }
    for (NSInteger i = 0; i < XCORECOUNT; i++) {
        for (NSInteger j = 0; j < YCORECOUNT - 1; j++) {
            Bus *bus = [[Bus alloc] init];
            CGRect frame = CGRectMake((i + 1)*Margin + (i * ProcessorWidth), (j + 1)*(Margin + ProcessorHeight), ProcessorWidth, Margin);
            UIView *view = [[UIView alloc] initWithFrame:frame];
            bus.view = view;
            [bus setupViewBetween:self.columns[i][j] lower:self.columns[i][j + 1]];
            [busses addObject:bus];
            [self.scrollView addSubview:view];
        }
    }
    self.input0 = [[InputBus alloc] initWithCaption:@"In0"];
    CGRect frame = CGRectMake((1 + 1)*Margin + (1 * ProcessorWidth), 0, ProcessorWidth, Margin);
    UIView *view;
    self.input0.view = view = [[UIView alloc] initWithFrame:frame];
    [self.input0 setupViewBetween:nil lower:self.columns[1][0]];
    [self.scrollView addSubview:view];
    
    self.input1 = [[InputBus alloc] initWithCaption:@"In1"];
    frame = CGRectMake((2 + 1)*Margin + (2 * ProcessorWidth), 0, ProcessorWidth, Margin);
    self.input1.view = view = [[UIView alloc] initWithFrame:frame];
    [self.input1 setupViewBetween:nil lower:self.columns[2][0]];
    [self.scrollView addSubview:view];
    
    self.output0 = [[OutputBus alloc] initWithCaption:@"Out0"];
    frame = CGRectMake((1 + 1)*Margin + (1 * ProcessorWidth), (YCORECOUNT)*(Margin + ProcessorHeight), ProcessorWidth, Margin);
    self.output0.view = view = [[UIView alloc] initWithFrame:frame];
    [self.output0 setupViewBetween:self.columns[1][YCORECOUNT - 1] lower:nil];
    [self.scrollView addSubview:view];
    
    self.output1 = [[OutputBus alloc] initWithCaption:@"Out1"];
    frame = CGRectMake((2 + 1)*Margin + (2 * ProcessorWidth), (YCORECOUNT)*(Margin + ProcessorHeight), ProcessorWidth, Margin);
    self.output1.view = view = [[UIView alloc] initWithFrame:frame];
    [self.output1 setupViewBetween:self.columns[2][YCORECOUNT - 1] lower:nil];
    [self.scrollView addSubview:view];
    
    self.videoOut = [[VideoBus alloc] init];
    self.videoOut.adapter = [[Video alloc] initWithFrame:[self videoFrame]];
    frame = CGRectMake((3 + 1)*Margin + (3 * ProcessorWidth), (YCORECOUNT)*(Margin + ProcessorHeight), ProcessorWidth, Margin);
    self.videoOut.view = view = [[UIView alloc] initWithFrame:frame];
    [self.videoOut setupViewBetween:self.columns[3][YCORECOUNT - 1] lower:nil];
    [self.scrollView addSubview:view];
    [self.scrollView addSubview:self.videoOut.adapter];
    
    self.objectives = [[UITextView alloc] initWithFrame:[self objectivesFrame]];
    self.objectives.font = [FontHelper defaultFont];
    self.objectives.selectable = NO;
    self.objectives.editable = NO;
    self.objectives.textColor = [UIColor grayColor];
    self.objectives.backgroundColor = [UIColor blackColor];
    [self.scrollView addSubview:self.objectives];
    
    [self basicViewController].computerProxy = self;
    self.busses = [busses copy];
    
    self.io = [[IO alloc] init];
    self.io.input0Data = self.input0;
    self.io.input1Data = self.input1;
    self.io.output0Data = self.output0;
    self.io.output1Data = self.output1;
    self.io.view = [[UIView alloc] initWithFrame:[self ioFrame]];
    [self.scrollView addSubview:self.io.view];
    [self reset];
}

- (CGFloat)kForZoom
{
    if (self.zoom) {
        if (self.zoom > 1)
            return 3.0;
        return 1.5;
    }
    return 1.0;
}

- (CGRect)processorFrameForX:(NSInteger)x Y:(NSInteger)y
{
    CGFloat k = [self kForZoom];
    return CGRectMake(((x + 1)*Margin + (x * ProcessorWidth))/k, ((y + 1)*Margin + (y * ProcessorHeight))/k, ProcessorWidth/k, ProcessorHeight/k);
}

- (CGSize)scrollViewContentSize
{
    CGFloat k = [self kForZoom];
    return CGSizeMake((ProcessorWidth * XCORECOUNT + Margin * (XCORECOUNT + 1) + IOWidth + Margin + VideoWidth + Margin)/k,
                      (ProcessorHeight * (YCORECOUNT + 1) + Margin * (YCORECOUNT + 1))/k);
}

- (CGRect)ioFrame
{
    CGFloat k = [self kForZoom];
    return CGRectMake((ProcessorWidth * XCORECOUNT + Margin * (XCORECOUNT + 1))/k, 0, IOWidth/k, (ProcessorHeight * YCORECOUNT + Margin * (YCORECOUNT + 1))/k);
}

- (CGRect)videoFrame
{
    CGFloat k = [self kForZoom];
    return CGRectMake((ProcessorWidth * XCORECOUNT + Margin * (XCORECOUNT + 2) + IOWidth)/k, Margin/k, VideoWidth/k, VideoWidth/k);
}

- (CGRect)objectivesFrame
{
    CGFloat k = [self kForZoom];
    return CGRectMake((ProcessorWidth * XCORECOUNT + Margin * (XCORECOUNT + 2) + IOWidth)/k, (VideoWidth + Margin)/k, VideoWidth/k, VideoWidth/k);
}

- (void)setObjectFrames {
    CGFloat k = [self kForZoom];
    [self.columns enumerateObjectsUsingBlock:^(NSArray *_Nonnull column, NSUInteger x, BOOL * _Nonnull stop) {
        [column enumerateObjectsUsingBlock:^(Processor *_Nonnull processor, NSUInteger y, BOOL * _Nonnull stop) {
            [processor.view setFrame:[self processorFrameForX:x Y:y]];
            [processor rebuildFramesWithK:k];
        }];
    }];
    for (Bus *bus in self.busses) {
        [bus rebuildFramesWithK:k defaultSpace:Margin/k];
    }
    [self.input0 rebuildFramesWithK:k defaultSpace:Margin/k];
    [self.input1 rebuildFramesWithK:k defaultSpace:Margin/k];
    [self.output0 rebuildFramesWithK:k defaultSpace:Margin/k];
    [self.output1 rebuildFramesWithK:k defaultSpace:Margin/k];
    [self.videoOut rebuildFramesWithK:k defaultSpace:Margin/k];
    [self.objectives setFrame:[self objectivesFrame]];
    [self.objectives setFont:[FontHelper defaultFontDecreasedBy:k]];
    [self.io.view setFrame:[self ioFrame]];
    [self.videoOut.adapter setFrame:[self videoFrame]];
    [self.io rebuildFramesWithK:k];
    [self.scrollView setContentSize:[self scrollViewContentSize]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (ViewController *)basicViewController
{
    id vc = self.parentViewController;
    while (vc) {
        if ([vc isKindOfClass:[ViewController class]]) return vc;
        vc = [vc parentViewController];
    }
    return nil;
}

#pragma mark - Processor Delegate Methods

- (void)processor:(Processor *)processor compilationError:(NSString *)errorMessage
{
//    [self basicViewController].statusLabel.text = errorMessage;
}

- (void)processorWillChangeProgram:(Processor *)processor
{
    [self reset];
}

#pragma mark - ComputerProxy Delegate Methods

- (StepResult)step:(NSString *__autoreleasing * _Nonnull)errorMessage
{
    NSInteger totalStepsWorked = 0;
    NSInteger totalStepCount = 0;
    for (NSArray *column in self.columns) {
        for (Processor *processor in column) {
            [processor step];
            totalStepsWorked += processor.stepsWorked;
            totalStepCount += processor.stepCount;
        }
    }
    for (Bus *bus in self.busses) {
        [bus stepIsOver];
    }
    for (NSArray *column in self.columns) {
        for (Processor *processor in column) {
            [processor stepIsOver];
        }
    }
    BusOutcome ioOutcome = [self.io step];
    if ((ioOutcome == OutputDoneValid) || (ioOutcome == OutputDoneInvalid)) {
        if (totalStepCount > 0) {
            *errorMessage = [NSString stringWithFormat:@"Processor utilization: %.0f%%", 100.0*totalStepsWorked/totalStepCount];
        }
        return StepProgramEnded;
    }
    return StepPerformed;
}

- (BOOL) hasProgramUpdates
{
    for (NSArray *column in self.columns) {
        for (Processor *processor in column) {
            if ([processor hasProgramUpdates])
                return YES;
        }
    }
    return NO;
}

- (void) reset
{
    for (NSArray *column in self.columns) {
        for (Processor *processor in column) {
            [processor reset];
        }
    }
    for (Bus *bus in self.busses) {
        [bus reset];
    }
    self.input0.data = [self.inData0 mutableCopy];
    self.input0.index = 0;
    self.input1.data = [self.inData1 mutableCopy];
    self.input1.index = 0;
    self.output0.desiredData = [self.outData0 mutableCopy];
    self.output0.realData = nil;
    self.output1.desiredData = [self.outData1 mutableCopy];
    self.output1.realData = nil;
    [self.io reset];
}

- (NSMutableDictionary *)programData
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    for (NSInteger i = 0; i < self.columns.count; i++) {
        NSArray *column = self.columns[i];
        NSMutableDictionary *columnDictionary = [NSMutableDictionary dictionary];
        for (NSInteger j = 0; j < column.count; j++) {
            Processor *processor = column[j];
            if (processor.mode == CoreModeStack) {
                columnDictionary[[NSString stringWithFormat:@"i%ldMode", (long)j]] = @"stack";
            } else {
                if (processor.programText) {
                    columnDictionary[[NSString stringWithFormat:@"i%ld", (long)j]] = processor.programText;
                }
            }
        }
        result[[NSString stringWithFormat:@"i%ld", (long)i]] = columnDictionary;
    }
    if (self.inData0) {
        result[@"In0"] = self.inData0;
    }
    if (self.inData1) {
        result[@"In1"] = self.inData1;
    }
    if (self.outData0) {
        result[@"Out0"] = self.outData0;
    }
    if (self.outData1) {
        result[@"Out1"] = self.outData1;
    }
    return result;
}

- (BOOL)validateNumberArray:(id _Nonnull)array withError:(NSError ** _Nonnull)error source:(NSString *)source
{
    if ([array isKindOfClass:[NSArray class]]) {
        for (id x in array) {
            if (![x isKindOfClass:[NSNumber class]]) {
                *error = [NSError errorWithDomain:@"Scenario JSON validateion" code:4 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@ has non-numeric data", source]}];
                return NO;
            }
        }
        if (((NSArray *)array).count) {
            return YES;
        }
        *error = [NSError errorWithDomain:@"Scenario JSON validateion" code:5 userInfo:@{NSLocalizedDescriptionKey:@"%@ is empty"}];
        return YES;
    }
    *error = [NSError errorWithDomain:@"Scenario JSON validateion" code:6 userInfo:@{NSLocalizedDescriptionKey:@"%@ is in wrong format"}];
    return NO;
}
    
- (BOOL) validateProgram:(NSDictionary *)programs withError:(NSError ** _Nonnull)error
{
    if (programs) {
        id i0 = programs[@"In0"];
        id i1 = programs[@"In1"];
        id o0 = programs[@"Out0"];
        id o1 = programs[@"Out1"];
        NSInteger nonEmptyIOCount = 0;
        if (i0 && ![self validateNumberArray:i0 withError:error source:@"In0"]) return NO;
        if (*error) {
            *error = nil;
        } else {
            nonEmptyIOCount++;
        }
        if (i1 && ![self validateNumberArray:i1 withError:error source:@"In1"]) return NO;
        if (*error) {
            *error = nil;
        } else {
            nonEmptyIOCount++;
        }
        if (o0 && ![self validateNumberArray:o0 withError:error source:@"Out0"]) return NO;
        if (*error) {
            *error = nil;
        } else {
            nonEmptyIOCount++;
        }
        if (o1 && ![self validateNumberArray:o1 withError:error source:@"Out1"]) return NO;
        if (*error) {
            *error = nil;
        } else {
            nonEmptyIOCount++;
        }
        if (nonEmptyIOCount == 0) {
            *error = [NSError errorWithDomain:@"Scenario JSON validateion" code:2 userInfo:@{NSLocalizedDescriptionKey: @"no input/output data"}];
        }
        BOOL gotOneRecord = NO;
        for (NSInteger i = 4; i--; ) {
            NSDictionary *columnDictionary = programs[[NSString stringWithFormat:@"i%ld", (long)i]];
            if (columnDictionary) {
                for (NSInteger j = 3; j--; ) {
                    NSString *processorProgram = columnDictionary[[NSString stringWithFormat:@"i%ld", (long)j]];
                    if (processorProgram) {
                        gotOneRecord = YES;
                        break;
                    }
                }
                if (gotOneRecord) break;
            }
        }
        if (gotOneRecord) {
            return YES;
        }
        *error = [NSError errorWithDomain:@"Scenario JSON validateion" code:1 userInfo:@{NSLocalizedDescriptionKey: @"no program records"}];
    } else {
        *error = [NSError errorWithDomain:@"Scenario JSON validateion" code:3 userInfo:@{NSLocalizedDescriptionKey:@"empty"}];
    }
    return NO;
}

- (void)loadFrom:(NSDictionary *)programs objectives:(NSString *)objectives
{
    if (programs) {
        self.inData0 = programs[@"In0"];
        self.inData1 = programs[@"In1"];
        self.outData0 = programs[@"Out0"];
        self.outData1 = programs[@"Out1"];
        if ((objectives == nil) || [objectives isEqualToString:@""]) {
            self.objectives.text = @"Free playground";
        } else {
            self.objectives.text = [NSString stringWithFormat:@"MISSION OBJECTIVE:\n\n%@", objectives];
        }
        [self reset];
        for (NSInteger i = 0; i < 4; i++) {
            NSDictionary *columnDictionary = programs[[NSString stringWithFormat:@"i%ld", (long)i]];
            NSArray *column = self.columns[i];
            for (NSInteger j = 0; j < column.count; j++) {
                Processor *processor = column[j];
                NSString *mode = columnDictionary[[NSString stringWithFormat:@"i%ldMode", (long)j]];
                NSString *processorProgram = columnDictionary[[NSString stringWithFormat:@"i%ld", (long)j]];
                if (mode && [mode isEqualToString:@"stack"]) {
                    processor.mode = CoreModeStack;
                } else {
                    processor.mode = CoreModeProcessor;
                    processor.programText = processorProgram?processorProgram:@"";
                }
            }
        }
    }
}

- (BOOL)running
{
    return NO;
#warning SHOW REAL VALUE
}

- (void)endEditing
{
    [self.scrollView endEditing:NO];
}

- (void)zoomOut
{
    if (self.zoom) {
        if (self.zoom == 1) {
            self.zoom = 2;
        } else {
            return;
        }
    } else {
        self.zoom = 1;
    }
    [self setObjectFrames];
}

- (void)zoomIn
{
    if (self.zoom) {
        if (self.zoom == 1) {
            self.zoom = 0;
        } else {
            self.zoom = 1;
        }
    } else {
        return;
    }
    [self setObjectFrames];
}

@end
